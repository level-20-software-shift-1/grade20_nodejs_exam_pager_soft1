

$(function () {
    $.get('http://127.0.0.1:8000/loginstates', (data) => {
        if (data == 200) {
            $.get('http://127.0.0.1:8000/head', (data) => {
                $('#top').append(data)

                $.get('http://127.0.0.1:8000/index', (data) => {
                    console.log(data);
                    log(data)
                })
            })
        }else{
            window.location.href='../front/login.html'
        }

    })
})

function log(data) {
    let Data = data.filter((item) => {
        return item.name !== undefined
    })

    Data.forEach(item => {
        let html = `
        <tr id ="html">
            <td>${item.id}</td>
            <td>${item.title}</td>
            <td>${item.thing1}</td>
            <td>${item.thing2}</td>
            <td>${item.thing3}</td>
            <td>${item.author}</td>
            <td>${item.time}</td>

            <td>
            <input type="button" value="编辑" onclick="Edit(${item.id})">
            <input type="button" value="删除" onclick="Delete(${item.id})">
            </td>
        </tr>
        `
        $('#content').append(html)
    });
}

function query() {

    let keyword = $('[name=keyword]').val()

    $.get('http://127.0.0.1:8000/data?keyword=' + keyword, (data) => {

        let tr = $('tr').length
        for (let i = 0; i < tr; i++) {
            $('#html').remove()
        }
        log(data)
    })
}

function add() {
    $.get('http://127.0.0.1:8000/addhtml', (data) => {

        $('#all').remove()

        $('#html').remove()
        
        $('#top').append(data)
    })
}

function save(Id) {

    let id = $('[name=id]').val()
    let title = $('[name=title]').val()
    let thing1 = $('[name=thing1]').val()
    let thing2 = $('[name=thing2]').val()
    let thing3 = $('[name=thing3]').val()
    let author = $('[name=author]').val()
    let time = $('[name=time]').val()
   

    let obj = {
        id,
        title,
        thing1,
        thing2,
        thing3,
        author,
        time
    }
    if (Id == undefined) {
        if (name=='') {
            alert('无法添加空值')
        }else{
            $.post('http://127.0.0.1:8000/indexadd', obj, (Data) => {
                
                    $('#add').remove()
                    $.get('http://127.0.0.1:8000/head', (data) => {
                    $('#top').append(data)
                    
                    log(Data)
                    
                })
                
                
            })
        }
        
    } else {

        $.post('http://127.0.0.1:8000/indexedit', obj, (Data) => {

            $('#add').remove()
            $.get('http://127.0.0.1:8000/head', (data) => {
                $('#top').append(data)
                log(Data)
            })
        })
    }
}

function Edit(id) {

    $.get('http://127.0.0.1:8000/index', (data) => {

        let Data = data.filter((item) => {
            return item.id == id
        })

        Data.forEach((item) => {
            let newId = item['id']
            let newTitle = item['title']
            let newThing1 = item['thing1']
            let newThing2 = item['thing2']
            let newThing3 = item['thing3']
            let newAuthor = item['author']
            let newTime = item['time']
           

            let obj = {
                newId,
                newTitle,
                newThing1,
                newThing2,
                newThing3,
                newAuthor,
                newTime
            }

            $.post('http://127.0.0.1:8000/addhtml', obj, (data) => {
                $('#all').remove()

                let tr = $('tr').length
                for (let i = 0; i < tr; i++) {
                    $('#html').remove()
                }
                $('#top').append(data)
            })
        })
    })
}

function Delete(id) {

    $.get('http://127.0.0.1:8000/indexDeleted?id=' + id, (data) => {
        console.log(data);
        let tr = $('tr').length
        for (let i = 0; i < tr; i++) {
            $('#html').remove()
        }
        log(data)
    })

}

function cancel(){
    $('#add').remove()

    $.get('http://127.0.0.1:8000/head', (data) => {
                $('#top').append(data)

                $.get('http://127.0.0.1:8000/index', (data) => {
                    log(data)
                })
            })
}