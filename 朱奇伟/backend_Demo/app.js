'use strict'

//需要引用的包
const koa = require('koa');
const cors = require('koa-cors');
const bodyparser = require('koa-bodyparser');
const router = require('./router');
const { Blog, sync } = require('./model');

let app = new koa();
sync().then(() => {
//     Blog.bulkCreate([
//         {
//             title: '论EF Core的自我修养',
//             digest: '论EF Core的自我修养',
//             text: '论EF Core的自我修养',
//             classify: '.NET',
//             author: 'InCerry',
//         },
//         {
//             title: 'DDD之我见',
//             digest: 'DDD之我见',
//             text: 'DDD之我见',
//             classify: '编程技术',
//             author: '某大神',
//         },
//         {
//             title: 'nginx负载平衡的几种策略',
//             digest: 'nginx负载平衡的几种策略',
//             text: 'nginx负载平衡的几种策略',
//             classify: '服务器',
//             author: '老胡来也',
//         },
//     ])
 })

app.use(cors());
app.use(bodyparser());
app.use(router());

app.listen(8080);