'use strict'

const fs = require('fs');
const router = require('koa-router')();

function registerRouter(router) {
    let obj = require('./blog');
    for (let key in obj) {
        let tmpArr = key.split(' ');
        let fn = obj[key];

        if (tmpArr[0] === 'get') {
            router.get(tmpArr[1], fn);
        } else if (tmpArr[0] === 'post') {
            router.post(tmpArr[1], fn);
        } else if (tmpArr[0] === 'put') {
            router.put(tmpArr[1], fn);
        } else if (tmpArr[0] === 'delete') {
            router.delete(tmpArr[1], fn);
        } else {
            console.log('请求失败');
        }
    }
    return router.routes();
}

module.exports = () => {
    let fn = registerRouter(router);
    return fn;
}