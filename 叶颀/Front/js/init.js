'use strict';

$(function () {
    getListWithKeyword();
})

function add() {
    console.log('你点了一下添加按钮');
    location.href = './addOrEdit.html';
}
// 
function getListWithKeyword(keyword) {
    keyword = keyword || ''
    console.log(keyword);
    getProductList(keyword).then(res => {
        console.log(res);
        render(res.data);
    })
}

// 渲染函数，根据传入的列表数据，重新渲染列表
function render(data) {
    let tb = $('#tbData');
    let rowData = $('.rowData');
    rowData.remove();
    data.forEach(item => {
        let h = `
            <tr class="rowData" key="${item.id}">
                <td>${item.id}</td>
                <td>${item.headline}</td>
                <td>${item.abstract}</td>
                <td>${item.content}</td>
                <td>${item.classify}</td>
                <td>${item.author}</td>
                <td>${item.time}</td>
                <td>
                <input type="button" value="修改" onclick="update(${item.id})">
                <input type="button" value="删除" onclick="del(${item.id})">
                </td>
            </tr>
            `
        tb.append(h);
    })
}