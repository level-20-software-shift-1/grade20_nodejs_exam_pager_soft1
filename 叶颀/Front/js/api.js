'use strict';


function getProductList(keyword) {
    return new Promise(function (resolve, reject) {
        $.get(`${baseUrl}/product?keyword=`+keyword, (data) => {
            resolve(data);
        })
    })
}


function getProductById(id) {
    return new Promise(function (abc, bbc) {
        $.get(`${baseUrl}/product/${id}`, data => {
            abc(data);
        })
    })
}


function addProduct(obj) {
    return new Promise(function (resolve, reject) {
        $.post(`${baseUrl}/product`, obj, (data) => {
            resolve(data);
        })
    })
}

function updateProduct(id, obj) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `${baseUrl}/product/${id}`,
            type: "PUT",
            data: obj,
            success: function (data) {
               
                resolve(data);
            }
        });
    })
}

function delProductById(id) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `${baseUrl}/product/${id}`,
            type: "delete",
            success: function (data) {
                
                resolve(data);
            }
        });
    })
}
