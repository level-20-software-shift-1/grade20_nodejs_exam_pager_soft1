'use strict';
const { DataTypes } = require('sequelize');

let model = {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    school:{
        type:DataTypes.STRING,
        allowNull:false,
        default:0
    },
    money:{
        type:DataTypes.TEXT,
        allowNull:false,
        default:100
    },
    grifriend:{
        type:DataTypes.STRING,
        allowNull:false
    }
    
}

module.exports=model;