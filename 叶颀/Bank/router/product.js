'use strict';
let { Products, Op } = require('../model');
let products = [
    {
        id:1,
        headline:'论EF Core的自我修养',
        abstract:'论EF Core的自我修养',
        content:'论EF Core的自我修养',
        classify:'.Net',
        author:'lnCerry',
        time:'2022-04-06 8:47'

    },
    {
        id:2,
        headline:'DDD之我见',
        abstract:'DDD之我见',
        content:'DDD之我见',
        classify:'编程技术',
        author:'某大神',
        time:'2022-04-03 23：47'
    },
    {
        id:3,
        headline:'nginx负载平衡的几种策略',
        abstract:'nginx负载平衡的几种策略',
        content:'nginx负载平衡的几种策略',
        classify:'服务器',
        author:'老胡来也',
        time:'2022-04-06 08:47'
    },
    {
        id:4,
        headline:'Linux用户创建的学习研究',
        abstract:'Linux用户创建的学习研究',
        content:'Linux用户创建的学习研究',
        classify:'Linux',
        author:'某大神',
        time:'2022-04-06 08:47'
    },
    {
      id:5,
      headline:'大数据仪表盘探讨',
      abstract:'大数据仪表盘探讨',
      content:'大数据仪表盘探讨',
      classify:'大数据',
      author:'居家博士',
      time:'2022-04-18 16:18'
  },
    
]

let fn_list = async (ctx, next) => {
    let keyword = ctx.request.query.keyword || '';
    console.log(keyword.length);
    if (keyword) {
        let list;
        list = await Products.findAll({
            where: {
                [Op.or]: [
                    { id: isNaN(keyword) ? 0 : parseInt(keyword) },
                    { name: keyword },
                    { school: isNaN(keyword) ? 0 : keyword },
                    { money: isNaN(keyword) ? 0 : parseInt(keyword) },
                    { grifriend: keyword },
                ]
            },
            order:['id']
        })
        ctx.body = {
            code: 1000,
            data: list,
            msg: '请求成功'
        }
    } else {
        let list = await Products.findAll({
            order:['id']
        });
        ctx.body = {
            code: 1000,
            data: list,
            msg: '请求成功'
        }
    }
}
let fn_getbyid = async (ctx, next) => {
    let id = ctx.request.params.id;
    let product = await Products.findByPk(id);
    if (product) {
        ctx.body = {
            code: 1000,
            data: product,
            msg: '获取成功'
        }
    } else {
        ctx.body = {
            code: 900,
            data: '',
            msg: '你查找的不存在'
        }
    }
}
let fn_post =async (ctx, next) => {
    let obj = ctx.request.body;
    console.log(obj);

    await Products.create(obj);
    ctx.body = obj;
}
let fn_put =async (ctx, next) => {
    let id = ctx.request.params.id;
    let obj = ctx.request.body;
    let product = await Products.findByPk(id);

    if (product) {
        Products.update(obj,{
            where:{
                id:id
            }
        })
        ctx.body = {
            code: 1000,
            data: product,
            msg: '修改成功'
        }
    } else {
        ctx.body = {
            code: 900,
            data: '',
            msg: '你所需要修改的不存在'
        }
    }
}
let fn_delete =async (ctx, next) => {
   
    let id = ctx.request.params.id;
    console.log(id);
    let product = await Products.findByPk(id);

    if (product) {
        Products.destroy({
            where:{
                id:id
            }
        })
        ctx.body = {
            code: 1000,
            data: { id },
            msg: '删除成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '要删除的元素或者记录不存在，请确认重试。'
        }
    }
}

module.exports = {
    'get /product': fn_list,
    'get /product/:id': fn_getbyid,
    'post /product': fn_post,
    'put /product/:id': fn_put,
    'delete /product/:id': fn_delete
}