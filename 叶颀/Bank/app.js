'use strict'

const koa = require('koa');
const cors = require('koa-cors');
const bodyparser = require('koa-bodyparser');

const routerMiddleware  =  require('./router/index')
const {Products,sync} = require('./model')

let app = new koa();


sync().then(()=>{
  Products.bulkCreate([
    {
        id:1,
        headline:'论EF Core的自我修养',
        abstract:'论EF Core的自我修养',
        content:'论EF Core的自我修养',
        classify:'.Net',
        author:'lnCerry',
        time:'2022-04-06 8:47'

    },
    {
        id:2,
        headline:'DDD之我见',
        abstract:'DDD之我见',
        content:'DDD之我见',
        classify:'编程技术',
        author:'某大神',
        time:'2022-04-03 23：47'
    },
    {
        id:3,
        headline:'nginx负载平衡的几种策略',
        abstract:'nginx负载平衡的几种策略',
        content:'nginx负载平衡的几种策略',
        classify:'服务器',
        author:'老胡来也',
        time:'2022-04-06 08:47'
    },
    {
        id:4,
        headline:'Linux用户创建的学习研究',
        abstract:'Linux用户创建的学习研究',
        content:'Linux用户创建的学习研究',
        classify:'Linux',
        author:'某大神',
        time:'2022-04-06 08:47'
    },
    {
      id:5,
      headline:'大数据仪表盘探讨',
      abstract:'大数据仪表盘探讨',
      content:'大数据仪表盘探讨',
      classify:'大数据',
      author:'居家博士',
      time:'2022-04-18 16:18'
  },
  ])
});


app.use(cors());
app.use(bodyparser());
app.use(routerMiddleware());

let port = 1000;
app.listen(port);
console.log(`本次服务器将运行在：http://localhost:${port}`);