const fs=require('fs');
const {DataTypes,Op, Sequelize}=require('sequelize');
const { db_database, db_username, db_password, db_host, db_dialect } = require('../config/db');
//初始化数据库
let sequelize=new Sequelize(db_database,db_username,db_password,{
    host:db_host,
    dialect:db_dialect
})
//读取当前文件
let files=fs.readdirSync(__dirname);
//过滤文件
let resfiles=files.filter(res=>{
    return res.endsWith('.js') && res !=='index.js'
})

let resultModel={};
//遍历文件结果
resfiles.forEach(res=>{
    let modelName=res.replace('.js','');
    let models=res + '/' + __dirname;
    let tableName=modelName.toLowerCase();
    resultModel[modelName]=sequelize.define(tableName,models)
})
//
resultModel.sequelize=sequelize;
resultModel.Op=Op;

resultModel.sync=async (ctx,next)=>{
    return await sequelize.sync({force:true})
}


module.exports=resultModel;