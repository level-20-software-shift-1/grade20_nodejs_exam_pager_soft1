'use strict'

function getBlogList(keyword){
    return new Promise(function(reslove,reject){
        $.get(`${baseUrl}/blog?keyword=`+keyword,data=>{
            reslove(data);
        })
    })
}


function getBlogById(id){
    return new Promise(function(reslove,reject){
        $.get(`${baseUrl}/blog/${id}`,data=>{
            reslove(data);
        })
    })
}


function addBlog(obj){
    return new Promise(function(reslove,reject){
        $.post(`${baseUrl}/blog`,obj,data=>{
            reslove(data);
        })
    })
}


function updateBlog(id,obj){
    return new Promise(function(reslove,reject){
        $.ajax({
            url:`${baseUrl}/blog/${id}`,
            type:'put',
            data:obj,
            success:function(data){
                reslove(data)
            }
        })
    })
}

function delBlogById(id){
    return new Promise(function(reslove,reject){
        $.ajax({
            url:`${baseUrl}/blog/${id}`,
            type:'delete',
            success:function(data){
                reslove(data)
            }
        })
    })
}