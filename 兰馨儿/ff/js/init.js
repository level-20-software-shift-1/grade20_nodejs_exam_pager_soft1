'use strict'

$(function(){
    getKeyword();
})

function getKeyword(keyword){
    keyword =keyword || '';
    getBlogList(keyword).then(res=>{
        console.log(res);
        rander =res.data;
    })
}

function rander(data){
    $('.row').remove();
    data.forEach(item => {
        let html =`
            <tr class="row" key="${item.id}">
                <td>${item.id}</td>
                <td>${item.title}</td>
                <td>${item.digest}</td>
                <td>${item.content}</td>
                <td>${item.classify}</td>
                <td>${item.author}</td>
                <td>${item.ptime}</td>
                <td>
                    <input type="button" value="修改" onclick="edit(${item.id})">
                    <input type="button" value="删除" onclick="del(${item.id})">
                </td>
        </tr>
        `
        $('#tb').append(html);
    });
}