'use strict'

const Koa =require('koa');
const cors =require('koa-cors');
const bodyParser =require('koa-bodyparser');
const routerM =require('./router/index');
const {Blogs,sync} =require('./model');
// sync().then(()=>{
//     Blogs.bulkCreate([
//         {
//             title:'论EF Core的自我修养',
//             digest:'论EF Core的自我修养',
//             content:'论EF Core的自我修养',
//             classify:'.Net',
//             author:'InCerry',
//             ptime:'2022-04-06 8:07'
//         },
//         {
//             title:'论EF Core的自我修养',
//             digest:'论EF Core的自我修养',
//             content:'论EF Core的自我修养',
//             classify:'.Net',
//             author:'InCerry',
//             ptime:'2022-04-06 8:07'
//         },
//         {
//             title:'论EF Core的自我修养',
//             digest:'论EF Core的自我修养',
//             content:'论EF Core的自我修养',
//             classify:'.Net',
//             author:'InCerry',
//             ptime:'2022-04-06 8:07'
//         }
//     ])
// });

let app =new Koa();
app.use(cors());
app.use(bodyParser());
app.use(routerM());

let port =8080;
app.listen(port);
console.log(`http://localhost:${port}`);