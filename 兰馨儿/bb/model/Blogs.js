'use strict'

const {DataTypes}=require('sequelize');

let model ={
    title:{
        type:DataTypes.STRING,
        allwNull:false
    },
    digest:{
        type:DataTypes.STRING,
        allwNull:false,
    },
    content:{
        type:DataTypes.STRING,
        allwNull:false
    },
    classify:{
        type:DataTypes.STRING,
        allwNull:false
    },
    author:{
        type:DataTypes.STRING,
        allwNull:false
    },
    author:{
        type:DataTypes.STRING,
        allwNull:false
    },
    ptime:{
        type:DataTypes.DATE,
        allwNull:false
    }
}

module.exports =model;