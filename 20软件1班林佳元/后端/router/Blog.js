'use strict'
let blogs=[
    {
        id:1,
        biaoti:'论EF Core的自我修养',
        zaiyao:'论EF Core的自我修养',
        neirong:'论EF Core的自我修养',
        fenlei:'.Net',
        zuozhe:'InCerry',
        shijian:'2022-04-06 08:47'
    },
    {
        id:2,
        biaoti:'DDD之我见',
        zaiyao:'DDD之我见',
        neirong:'DDD之我见',
        fenlei:'编程技术',
        zuozhe:'某大神',
        shijian:'2022-04-03 23:47'
    },
    {
        id:3,
        biaoti:'nginx负载平衡的几种策略',
        zaiyao:'nginx负载平衡的几种策略',
        neirong:'nginx负载平衡的几种策略',
        fenlei:'服务器',
        zuozhe:'老胡来也',
        shijian:'2022-04-06 08:47'
    },
    {
        id:4,
        biaoti:'Linux用户创建的学习研究',
        zaiyao:'Linux用户创建的学习研究',
        neirong:'Linux用户创建的学习研究',
        fenlei:'Linux',
        zuozhe:'某大神',
        shijian:'2022-04-06 08:47'
    },
    {
        id:5,
        biaoti:'大数据仪表盘探讨',
        zaiyao:'大数据仪表盘探讨',
        neirong:'大数据仪表盘探讨',
        fenlei:'大数据',
        zuozhe:'居家博士',
        shijian:'2022-04-18 16:18'
    },
]


let fn_list=(ctx,next)=>{
    let keyword=ctx.request.query.keyword||''
    if(keyword){
        let app=blogs.filter(item=>{
            return item.id==keyword || item.biaoti==keyword || item.zaiyao==keyword || item.neirong==keyword || item.fenlei==keyword || item.zuozhe==keyword || item.shijian==keyword
        })
        ctx.body={
            code:1000,
            data:app,
            msg:'请求成功'
        }
    }else{
        ctx.body={
            code:1000,
            data:blogs,
            msg:'请求成功'
        }
    }
}

let fn_getById=(ctx,next)=>{
    let id=ctx.request.params.id
    let filterList=blogs.filter(item=>{
        return item.id==id
    })
    if(filterList.length>0){
        ctx.body={
            code:1000,
            data:filterList[0],
            msg:'获取指定id成功'
        }
    }else{
        ctx.body={
            code:'400',
            data:'',
            msg:'元素不存在'
        }
    }
}

let fn_post=(ctx,next)=>{
    let obj=ctx.request.body
    obj.id=blogs[blogs.length-1].id+1
    blogs.push(obj)
    ctx.body=obj
}

let fn_put=(ctx,next)=>{
    let id=ctx.request.params.id
    let obj=ctx.request.body
    let filterList=blogs.filter(item=>{
        return item.id==id
    })
    if(filterList.length>0){
        let tmpObj=filterList[0]
        tmpObj.biaoti=obj.biaoti
        tmpObj.zaiyao=obj.zaiyao
        tmpObj.neirong=obj.neirong
        tmpObj.fenlei=obj.fenlei
        tmpObj.zuozhe=obj.zuozhe
        tmpObj.shijian=obj.shijian
        ctx.body={
            code:1000,
            data:tmpObj,
            msg:'修改成功'
        }
    }else{
        ctx.body={
            code:400,
            data:'',
            msg:'元素不存在'
        }
    }
}

let fn_delete=(ctx,next)=>{
    let id=ctx.request.params.id
    let arr=blogs.filter(item=>{
        return item.id != id
    })
    if(arr.length<blogs.length){
        blogs=arr
        ctx.body={
            code:1000,
            data:{id},
            msg:'删除成功'
        }
    }else{
        ctx.body={
            code:400,
            data:null,
            msg:'元素不存在'
        }
    }
}

module.exports={
    'get /Blog':fn_list,
    'get /Blog/:id':fn_getById,
    'post /Blog':fn_post,
    'put /Blog/:id':fn_put,
    'delete /Blog/:id':fn_delete
}