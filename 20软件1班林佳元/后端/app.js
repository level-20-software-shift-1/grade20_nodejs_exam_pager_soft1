'use strict'
const Koa=require('koa')
const bodyParser=require('koa-bodyparser')
const cors=require('koa-cors')
const routerMiddle=require('./router/index')
const {Blogs,sync} = require('./model/index')

/* sync().then(()=>{
    Blogs.bulkCreate([
        {
            biaoti:'论EF Core的自我修养',
            zaiyao:'论EF Core的自我修养',
            neirong:'论EF Core的自我修养',
            fenlei:'.Net',
            zuozhe:'InCerry',
            shijian:'2022-04-06 08:47'
        },
        {
            biaoti:'DDD之我见',
            zaiyao:'DDD之我见',
            neirong:'DDD之我见',
            fenlei:'编程技术',
            zuozhe:'某大神',
            shijian:'2022-04-03 23:47'
        },
        {
            biaoti:'nginx负载平衡的几种策略',
            zaiyao:'nginx负载平衡的几种策略',
            neirong:'nginx负载平衡的几种策略',
            fenlei:'服务器',
            zuozhe:'老胡来也',
            shijian:'2022-04-06 08:47'
        },
        {
            biaoti:'Linux用户创建的学习研究',
            zaiyao:'Linux用户创建的学习研究',
            neirong:'Linux用户创建的学习研究',
            fenlei:'Linux',
            zuozhe:'某大神',
            shijian:'2022-04-06 08:47'
        },
        {
            biaoti:'大数据仪表盘探讨',
            zaiyao:'大数据仪表盘探讨',
            neirong:'大数据仪表盘探讨',
            fenlei:'大数据',
            zuozhe:'居家博士',
            shijian:'2022-04-18 16:18'
        },
          ])
}) */

let app=new Koa()


app.use(cors())
app.use(bodyParser())
app.use(routerMiddle())


let port=3000
app.listen(port)
console.log(`web服务器运行在：http://localhost:${port}`);