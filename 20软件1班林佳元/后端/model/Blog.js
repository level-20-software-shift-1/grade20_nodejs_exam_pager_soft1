'use strict';
const { DataTypes } = require('sequelize');

let model = {
    biaoti: {
        type: DataTypes.STRING,
        allowNull: false
    },
    zaiyao:{
        type:DataTypes.DECIMAL,
        allowNull:false,
        default:0
    },
    neirong:{
        type:DataTypes.INTEGER,
        allowNull:false,
        default:100
    },
    fenlei:{
        type:DataTypes.STRING,
        allowNull:false
    },
    zuozhe:{
        type:DataTypes.STRING,
        allowNull:false
    }
}

module.exports=model;