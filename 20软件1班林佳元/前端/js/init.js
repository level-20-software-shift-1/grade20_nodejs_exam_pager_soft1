'use strict'

$(function(){
    getListWithKeyWord()
})

function add(){
    location.href='./addOrEdit.html'
}

function getListWithKeyWord(keyword){
    keyword=keyword||''
    getBlogList(keyword).then(res=>{
        render(res.data)
    })
}

function render(data){
    let tb=$('#tbData')
    let rowData=$('.rowData')
    rowData.remove()
    console.log(data);
    data.forEach(item => {
        let h=`
            <tr class="rowData" key="${item.id}">
            <td>${item.id}</td>
            <td>${item.biaoti}</td>
            <td>${item.zaiyao}</td>
            <td>${item.neirong}</td>
            <td>${item.fenlei}</td>
            <td>${item.zuozhe}</td>
            <td>${item.shijian}</td>
            <td>
            <input type="button" value="修改" onclick="update(${item.id})">
                <input type="button" value="删除" onclick="del(${item.id})">
            </td>
            </tr>
        `
        tb.append(h)
    });
}