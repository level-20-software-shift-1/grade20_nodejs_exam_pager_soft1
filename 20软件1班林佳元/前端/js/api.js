'use strict'
function getBlogList(keyword){
    return new Promise(function(resolve,reject){
        $.get('http://localhost:3000/Blog?keyword='+keyword,(data)=>{
            resolve(data)
        })
    })
}

function getBlogById(id){
    return new Promise(function(resolve,reject){
        $.get(`http://localhost:3000/Blog/${id}`,data=>{
            resolve(data)
        })
    })
}

function addBlog(obj){
    return new Promise(function(resolve,reject){
        $.post('http://localhost:3000/Blog',obj,(data)=>{
            resolve(data)
        })
    })
}

function updateBlog(id,obj){
    return new Promise(function(resolve,reject){
        $.ajax({
            url:`http://localhost:3000/Blog/${id}`,
            type:"PUT",
            data:obj,
            success:function(data){
                resolve(data)
            }
        })
    })
}

function delBlog(id){
    return new Promise(function(resolve,reject){
        $.ajax({
            url:`http://localhost:3000/Blog/${id}`,
            type:"DELETE",
            success:function(data){
                resolve(data)
            }
        })
    })
}