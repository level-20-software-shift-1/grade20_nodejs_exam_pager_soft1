'use strict';
const { DataTypes } = require('sequelize');

let model = {
    // Model attributes are defined here
    username: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING
        // allowNull defaults to true
    }
}

module.exports=model;