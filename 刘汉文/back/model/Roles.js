'use strict';
const { DataTypes } = require('sequelize');

let model = {
    // Model attributes are defined here
    roleName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
}

module.exports=model;