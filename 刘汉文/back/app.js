
'use strict';

const Koa = require('koa');
const cors = require('koa-cors');

const bodyParser = require('koa-bodyparser');
const routerMiddleware = require('./router/index');



const {Products,sync}= require('./model');
let app=new Koa();
app.use(bodyParser());
app.use(cors());

app.use(routerMiddleware());

let port = 3000;

app.listen(port);

console.log(`服务器运行在如下地址：http://localhost:${port}`);