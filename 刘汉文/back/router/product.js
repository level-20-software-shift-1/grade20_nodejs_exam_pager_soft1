'use strict';
let { Products, Op } = require('../model');
let products = [
    {
        id: 1,
        productName: '论EFcore的自我修养',
        neirong:'论EFcore的自我修养',
        zyao:'论EFcore的自我修养',
        fenlei: '.net',
        zze: 'Incerry'
    },
    {
        id: 2,
        productName: 'DD之我之剑',
        neirong:'DD之我之剑',
        zyao:'DD之我之剑',
        fenlei: '编程技术',
        zze: '某大神'
    },
    {
        id: 3,
        productName: '负载平衡的。。。',
        neirong:'负载平衡的。。。',
        zyao:'负载平衡的。。。',
        fenlei: '服务器',
        zze: '螃蟹天堂'
    },
    {
        id: 4,
        productName: '大数据',
        neirong:'大数据',
        zyao:'大数据',
        fenlei: 'Linux',
        zze: '肥佬'
    },
    {
        id: 5,
        productName: '如何找到富婆',
        neirong:'如何找到富婆',
        zyao:'如何找到富婆',
        fenlei: '成功之路',
        zze: '小马'
    },
    
   
]

let fn_list = async (ctx, next) => {
  

    let keyword = ctx.request.query.keyword || '';
    console.log(keyword.length);
    //判断关键字是否为空
    if (keyword) {
        let list;
        
        list = await Products.findAll({
            where: {
                [Op.or]: [
                    { id: isNaN(keyword) ? 0 : parseInt(keyword) },
                    { productName: keyword },
                    { neirong: isNaN(keyword) ? 0 : keyword },
                    { zyao: keyword },
                    { fenlei: isNaN(keyword) ? 0 : parseInt(keyword) },
                    { zze: keyword },
                ]
            },
            order:['id']
        })
        ctx.body = {
            code: 1000,
            data: list,
            msg: '请求成功'
        }
    } else {
        let list = await Products.findAll({
            order:['id']
        });
        ctx.body = {
            code: 1000,
            data: list,
            msg: '请求成功'
        }
    }
}
let fn_getbyid = async (ctx, next) => {
    let id = ctx.request.params.id;
    let product = await Products.findByPk(id);
    if (product) {
        ctx.body = {
            code: 1000,
            data: product,
            msg: '获取指定内容成功'
        }
    } else {
        ctx.body = {
            code: 900,
            data: '',
            msg: '你查找的内容不存在'
        }
    }
}
let fn_post =async (ctx, next) => {
    let obj = ctx.request.body;
    console.log(obj);

    await Products.create(obj);
    ctx.body = obj;
}

let fn_put =async (ctx, next) => {
    // 获取id
    let id = ctx.request.params.id;

    let obj = ctx.request.body;
    let product = await Products.findByPk(id);
    
    // 如果找到了对应的记录，则进行修改;否则提示查找的记录不存在
    if (product) {
        Products.update(obj,{
            where:{
                id:id
            }
        })
        ctx.body = {
            code: 1000,
            data: product,// 返回修改成功以后的对象
            msg: '修改内容成功'
        }
    } else {
        ctx.body = {
            code: 900,
            data: '',
            msg: '你所需要修改的内容不存在'
        }
    }
}
let fn_delete =async (ctx, next) => {
  
    let id = ctx.request.params.id;
    console.log(id);
    let product = await Products.findByPk(id);
    
    if (product) {
     
        Products.destroy({
            where:{
                id:id
            }
        })
        ctx.body = {
            code: 1000,
            data: { id },
            msg: '删除成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '要删除的记录不存在，请确认重试。'
        }
    }
}


module.exports = {
    'get /product': fn_list,
    'get /product/:id': fn_getbyid,
    'post /product': fn_post,
    'put /product/:id': fn_put,
    'delete /product/:id': fn_delete
}