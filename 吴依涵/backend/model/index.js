'use strict'

const {Sequelize,DataTypes,Op} = require('sequelize');
const fs = require('fs');
const {db_dialect,db_host,db_username,db_password,db_database} = require('../config/db');
const sequelize = new Sequelize(db_database,db_username,db_password,{
    host:db_host,
    dialect:db_dialect
});

//连接数据库
let Files = fs.readdirSync(__dirname);
let resFiles = Files.filter(item => {
    return item.endsWith('.js') && item !== 'index.js';
});

let resultModel = {};

resFiles.forEach(item=>{
    let modelName = item.replace('.js','');
    let model = require(__dirname + '/' + item);
    let tableName = modelName.toLowerCase();
    resultModel[modelName] = sequelize.define(tableName,model);
});


//挂载
resultModel.sequelize = sequelize;
resultModel.Op = Op;
resultModel.sync = async()=>{
    return await sequelize.sync({force:true});
}

module.exports=resultModel;