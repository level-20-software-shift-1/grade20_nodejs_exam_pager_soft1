'use strict'

const {DataTypes} = require('sequelize');

//定义模型
let model = {
    Title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    Abstract:{
        type:DataTypes.STRING,
        allowNull:false
    },
    Content:{
        type:DataTypes.STRING,
        allowNull:false
    },
    Classify:{
        type:DataTypes.STRING,
        allowNull:false
    },
    Author:{
        type:DataTypes.STRING,
        allowNull:false
    },
    ReleaseTime:{
        type:DataTypes.STRING,
        allowNull:false
    }
};

module.exports=model;