'use strict'

const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const cors = require('koa-cors');
const router = require('./routers/index');
const { Products, sync } = require('./model');

let app = new Koa();

//注册路由
app.use(bodyParser());
app.use(cors());
app.use(router());

sync().then(() => {//初始化数据(批量添加)
    Products.bulkCreate([
        {
            Title: "我是标题1",
            Abstract: "我是摘要1",
            Content: "我是内容1",
            Classify: "我是分类1",
            Author: "我是作者1",
            ReleaseTime: "2020 - 10 - 11"
        },
        {
            Title: "我是标题12",
            Abstract: "我是摘要2",
            Content: "我是内容2",
            Classify: "我是分类2",
            Author: "我是作者2",
            ReleaseTime: "2020 - 10 - 11"
        },
        {
            Title: "我是标题3",
            Abstract: "我是摘要3",
            Content: "我是内容3",
            Classify: "我是分类3",
            Author: "我是作者3",
            ReleaseTime: "2020 - 10 - 11"
        }
    ]);
});

let port = 3000;

app.listen(port);

console.log(`点击进入：http://localhost:${port}`);