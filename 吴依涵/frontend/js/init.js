'use strict'

$(function () {
    selectProduct();
});

function selectProduct(keyword) {
    keyword = keyword || '';
    getProductList(keyword).then(res=>{
        render(res.data);
    });
}

function Add() {//添加跳转
    location.href = "./add.html";
}

function render(data) {//渲染主页
    let table = $('#table');
    let row = $('.row');
    row.remove();
    data.forEach(item => {
        let html = `
        <tr class="row" key="${item.id}">
        <td>${item.id}</td>
        <td>${item.Title}</td>
        <td>${item.Abstract}</td>
        <td>${item.Content}</td>
        <td>${item.Classify}</td>
        <td>${item.Author}</td>
        <td>${item.ReleaseTime}</td>
        <td>
            <input type="button" value="编辑" onclick="Update(${item.id})">
            <input type="button" value="删除" onclick="Delete(${item.id})">
        </td>
        </tr>
        `
        table.append(html);
    });
}