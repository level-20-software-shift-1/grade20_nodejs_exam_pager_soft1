'use strict'

const {DataTypes}=require("sequelize")

let model={
    title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    abstract:{
        type:DataTypes.STRING,
        allowNull:false
    },
    content :{
        type:DataTypes.STRING,
        allowNull:false
    },
    classify:{
        type:DataTypes.STRING,
        allowNull:false
    },
    autor:{
        type:DataTypes.STRING,
        allowNull:false
    },
    postedtTime:{
        type:DataTypes.STRING,
        allowNull:false
    }
}

module.exports=model;

