'use strict'

const Koa = require("koa");
const cors = require("koa-cors");
const bodyparser = require("koa-bodyparser");
const router = require("koa-router")();
let model = require("./blogs");

const { Sequelize, Op } = require("sequelize");

const sequelize = new Sequelize("postgres", "postgres", "XpzLjj190126", {
  host: "19lxxpz.top",
  dialect:"postgres"
});

let blogs = sequelize.define("blogs",model)

// sequelize.sync({force: true }).then(
//     ()=>{
//         blogs.bulkCreate([
//             {
//                 title:"论自我修养",
//                 abstract:"论自我修养",
//                 content:"类容",
//                 classify:".net",
//                 autor:"IN",
//                 postedtTime:"2022-4.4"
//               },
//               {
//                 title:"论自我修养",
//                 abstract:"ddd",
//                 content:"dddd",
//                 classify:".dd",
//                 autor:"IddN",
//                 postedtTime:"2022-4.4"
//               },
//               {
//                 title:"自我修养",
//                 abstract:"ddd",
//                 content:"dddd",
//                 classify:".dd",
//                 autor:"IddN",
//                 postedtTime:"2022-4.4"
//               },
//         ])
//         }
// )
let app=new Koa();

//获取列表
router.get("/blogs", async (ctx) => {
  let list = await blogs.findAll({
    order: ["id"]
  })
  ctx.body = {
    data: list
  }

})
//新增
router.post("/blogs", async (ctx) => {
  let { title, abstract, content, classify,autor,postedtTime } = ctx.request.body
  await blogs.create({ title: title, abstract: abstract, content: content, classify: classify,autor:autor,postedtTime:postedtTime })

  ctx.body = await blogs.findAll();

})

//删除
router.delete("/blogs/:id", async (ctx) => {
  let id = ctx.request.params.id;
  console.log(id);
  await blogs.destroy({
    where: {
      id: id
    }
  });

  ctx.body = '删除成功';
})


app.use(cors())
app.use(bodyparser())
app.use(router.routes())



let post=3000;
app.listen(post);