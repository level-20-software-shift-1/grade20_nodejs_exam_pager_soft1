$(function(){
    console.log('加载成功');

    $.get(`http://localhost:8888/blog`,data=>{

        let list=data.data;
        console.log(list);

        let tb=$('table')
        
        list.forEach(item=>{
            let html=`
            <tr>
                <td>${item.id}</td>
                <td>${item.title}</td>
                <td>${item.digest}</td>
                <td>${item.content}</td>
                <td>${item.sort}</td>
                <td>${item.author}</td>
                <td>${item.createdAt}</td>
                <td>
                <input type="button" value="编辑" onclick="update(${item.id})">
                <input type="button" value="删除" onclick="del(${item.id})">
                </td>
            </tr>
            `
            tb.append(html);
        })
           
    })

})


function add(){
    window.location.href='./add.html'
}

function find(){
    console.log('查找');
    let keyword=$('[name=find]').val();
    console.log(keyword);

    $.get(`http://localhost:8888/blog?keyword=${keyword}`,data=>{
        let list=data.data;
        console.log(list);

        let tb=$('table')
        tb.html(`
                <tr>
                    <td>ID</td>
                    <td>标题</td>
                    <td>摘要</td>
                    <td>内容</td>
                    <td>分类</td>
                    <td>作者</td>
                    <td>发表时间</td>
                    <td>事件</td>
                </tr>

        `)
        list.forEach(item=>{
            let html=`
            <tr>
                <td>${item.id}</td>
                <td>${item.title}</td>
                <td>${item.digest}</td>
                <td>${item.content}</td>
                <td>${item.sort}</td>
                <td>${item.author}</td>
                <td>${item.createdAt}</td>
                <td>
                <input type="button" value="编辑" onclick="update(${item.id})">
                <input type="button" value="删除" onclick="del(${item.id})">
                </td>
            </tr>
            `
            tb.append(html);

        })

    })

}

function del(id){
    if(confirm('你确定要删除吗？')){
        $.ajax({
            url:`http://localhost:8888/blog/${id}`,
            type:'delete',
            success:function(data){
                window.location.href='./index.html'
            }
        })    
    }

    
}

function update(id){
    window.location.href=`./update.html?${id}`
}
