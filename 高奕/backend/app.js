'use strict';

const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const router = require('koa-router')();
const cors = require('koa-cors');
const { Sequelize,Op } = require('sequelize');

const app = new Koa();

const sequelize = new Sequelize('testdemo', 'postgres', 'postgres123456', {
    host: '120.25.236.62',
    dialect: 'postgres'
})
const Model = require('./model/Blog');
const Blog = sequelize.define('blog', Model);

// sequelize.sync({force:true}).then(()=>{
//     Blog.bulkCreate([
//         {
//             title:'标题',
//             digest:'这是摘要',
//             content:'无法显示',
//             sort:'分类',
//             author:'我',
//             createTime:'2002/05/01',
//             isDelete:0
//         }, {
//             title:'标题',
//             digest:'这是摘要',
//             content:'无法显示',
//             sort:'分类',
//             author:'我',
//             createTime:'2002/05/01',
//             isDelete:0
//         }, {
//             title:'标题',
//             digest:'这是摘要',
//             content:'无法显示',
//             sort:'分类',
//             author:'我',
//             createTime:'2002/05/01',
//             isDelete:0
//         }
//     ])

// })

router.get('/blog', async (ctx, next) => {

    let keyword=ctx.request.query.keyword;
    console.log(keyword);

    if(keyword &&keyword.length>0){
        let blog=await Blog.findAll({
            where:{
                [Op.or]:[
                    {id:isNaN(keyword)?0:parseInt(keyword)},
                    {author:keyword},
                    {content:keyword},
                    {digest:keyword},
                    {sort:keyword},
                    {title:keyword},
                ],
                isDelete:0,
            },
            order:['id']
        })
        
        ctx.body={
            code:'1000',
            data:blog,
            msg:'查询成功'
        }

    }else{
        let list = await Blog.findAll({
            where: {
                isDelete: 0
            },order:['id']
        })
    
        ctx.body = {
            code: '1000',
            data: list,
            msg: '请求成功'
        }
    }
   
    // let list = await Blog.findAll({
    //     where: {
    //         isDelete: 0
    //     },order:['id']
    // })

    // ctx.body={
    //     code:'1000',
    //     data:list,
    //     msg:'请求成功'

    // }

});

router.get('/blog/:id', async (ctx, next) => {

    let id=ctx.params.id
    console.log(id);

    let blog=await Blog.findAll({
        where:{
            id:id
        }
    })

    ctx.body={
        code:'1000',
        data:blog[0],
        msg:'获取成功'
    }
});

router.put('/blog/:id', async (ctx, next) => {

    let id=ctx.params.id;
    console.log(id);
    let obj=ctx.request.body
    console.log(obj);
   
    let blog=await Blog.update({
        title:obj.title,
        digest:obj.digest,
        content:obj.content,
        sort:obj.sort,
        author:obj.author
    },{
        where:{
            id:id
        }
    }
    )

    ctx.body={
        code:"1000",
        data:'',
        msg:'更新成功'
    }

});

router.delete('/blog/:id', async (ctx, next) => {

    let id=ctx.params.id    
    console.log(id);
    let blog=await Blog.update({isDelete:1},{
        where:{
            id:id
        },
        order:['id']
    })

    ctx.body={
        code:"1000",
        data:'',
        msg:'删除成功'
    }
});

router.post('/blog', async (ctx, next) => {

    let obj=ctx.request.body;
    obj.isDelete=0;
    console.log(obj);

    let blog=await Blog.create(obj)

    ctx.body={
        code:'1000',
        data:blog[0],
        msg:'添加成功'
    }
});



app.use(cors());
app.use(bodyParser());
app.use(router.routes());


let port = 8888;
app.listen(port);
console.log(`服务运行在 http://localhost:8888`);
