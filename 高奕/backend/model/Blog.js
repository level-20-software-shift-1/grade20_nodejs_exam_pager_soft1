const {DataTypes}=require('sequelize');

let model={
    title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    digest:{
        type:DataTypes.STRING,
        allowNull:false
    },
    content:{
        type:DataTypes.STRING,
        allowNull:false
    },
    sort:{
        type:DataTypes.STRING,
        allowNull:false
    },
    author:{
        type:DataTypes.STRING,
        allowNull:false
    },
    isDelete:{
        type:DataTypes.INTEGER,
        allowNull:false
    }

}


module.exports=model;