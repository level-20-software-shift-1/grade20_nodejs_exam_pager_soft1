'use strict';
$(function () {
    getListWithKeyword();
})
function add() {
    console.log('点击了一次新增按钮');
    location.href = './addOrEdit.html';
}
function getListWithKeyword(keyword) {
    keyword = keyword|| ''
    console.log(keyword);
    getProductList(keyword).then(res =>{
        console.log(res);
        render(res.data);   
    })
}
function render(data) {
    let tb = $('#taData');
    let rowData = $('.rowData');
    rowData.remove();
    data.forEach(item => {
       let h = `
       <tr class="rowData" key="${item.id}">
                <td>${item.biaoti}</td>
                <td>${item.zhaiyao}</td>
                <td>${item.neirong}</td>
                <td>${item.fenlei}</td>
                <td>${item.zuozhe}</td>
                <td>
                <input type="button" value="修改" onclick="xiugai(${item.id})">
                <input type="button" value="删除" onclick="shanchu(${item.id})">
                </td>
            </tr>
       `
       tb.append(h);
    });
}