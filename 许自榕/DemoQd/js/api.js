'use strict';
function getProductList(keyword) {
    return new Promise(function (resolve, reject) {
    $.get('http://localhost:3000/product?biaoti'+keyword,(data)=>{
        resolve(data);
    })
    })
}
function getProductById(id) {
    return new Promise(function (ccb, bbc) {
    $.get(`http://localhost:3000/product/${id}`,data=>{
        ccb(data);
    })
    })
}
function addProduct(obj) {
    return new Promise(function (resolve, reject) {
    $.get('http://localhost:3000/product',obj,(data)=>{
        resolve(data);
    })
    })
}
function updateProduct(id,obj) {
    return new Promise(function (resolve, reject) {
    $.ajax({
        url:`http://localhost:3000/product/${id}`,
        type:"put",
        data: obj,
        success:function(data){

            resolve(data);
        }
    });
    })
}
function delProductById(id) {
    return new Promise(function (resolve, reject) {
    $.ajax({
        url:`http://localhost:3000/product/${id}`,
        type:"delete",
        success:function(data){
            resolve(data);
        }
    });
    })
}