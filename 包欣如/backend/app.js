'use strict';

const Koa = require('koa');
const router = require('koa-router')();
const bodyparser = require('koa-bodyparser');
const cors = require('cors')
const Sequelize= require('sequelize');

let app = new Koa();


let sequelize = new Sequelize('postgres','postgres','123',{
    host:'baoxinru.top',     //120.76.228.84
    dialect:'postgres'
})

const product=require('./model/Product');
let Products = sequelize.define('products',product);

sequelize.sync({force:true}).then(()=>{
    Products.bulkCreate([
        {
            productName:'xixi',
            price:230
        },
        {
            productName:'lala',
            price:230
        }
    ])
});





let products = [
    {
        id: 1,
        title: '如何成为美女',
        digest: '培养美女',
        content: '没有内容',
        writer: '美女本女',
        publishtime: '2022.4.8',
        classification: '美女类'

    },
    {
        id: 2,
        title: '如何成为帅哥',
        digest: '培养帅哥',
        content: '老胡是个帅哥',
        writer: '老胡本胡',
        publishtime: '2022.4.8',
        classification: '帅哥类'
    },
    {
        id:3 ,
        title: '如何成为白富美',
        digest: '培养白富美',
        content: '没有内容',
        writer: '白富美本富',
        publishtime: '2022.4.8',
        classification: '富婆类'
    },
]

    //这是增

    router.post('product',async(ctx,next)=>{
        let obj = ctx.request.body;
    console.log(obj);

    await Products.create(obj);
    ctx.body = obj;
    });

    //这是删
    router.delete('product/:id',async(ctx,next)=>{
        let id = ctx.request.params.id;
    console.log(id);
    let product = await Products.findByPk(id);
 
    if (product) {
   
        Products.destroy({
            where:{
                id:id
            }
        })
        ctx.body = {
            code: 500,
            data: { id },
            msg: '你删除成功啦'
        }
    } else {
        ctx.body = {
            code: 300,
            data: null,
            msg: 'bbq了你要删除的东西不存在哦。'
        }
    }
    });

    //这是改

    router.put('product/:id',async(ctx,next)=>{
        let id = ctx.request.params.id;
        let obj = ctx.request.body;
        let product = await Products.findByPk(id);
        
        if (product) {
            Products.update(obj,{
                where:{
                    id:id
                }
            })
            ctx.body = {
                code: 900,
                data: product,
                msg: '滴滴，你的修改已成功'
            }
        } else {
            ctx.body = {
                code: 800,
                data: '',
                msg: 'bbq了你要修改的东西不存在哦'
            }
        }
    });

    //这是查
    
    router.get('product/:id',async(ctx,next)=>{
        let id = ctx.request.params.id;
    let product = await Products.findByPk(id);
    if (product) {
        ctx.body = {
            code: 900,
            data: product,
            msg: '呀，获取商品成功~'
        }
    } else {
        ctx.body = {
            code: 900,
            data: '',
            msg: 'bbq了你要查找的东西不存在哦'
        }
    }
    });



app.use(cors());
app.use(bodyparser());
app.use(router.routes());

let port = 6000;
app.listen(port);
console.log(`服务器运行在如下地址：http//localhost:${port}`);