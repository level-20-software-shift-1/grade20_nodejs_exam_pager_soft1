'use strict';
let{DataTypes}=require('sequelize');

let model = {
    productName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    price:{
        type:DataTypes.DECIMAL,
        allowNull:false,
        default:0
    },
    stockNum:{
        type:DataTypes.INTEGER,
        allowNull:false,
        default:100
    },
    supplier:{
        type:DataTypes.STRING,
        allowNull:false
    }
}

module.exports = {
    'get /product': fn_list,
    'get /product/:id': fn_getbyid,
    'post /product': fn_post,
    'put /product/:id': fn_put,
    'delete /product/:id': fn_delete
}