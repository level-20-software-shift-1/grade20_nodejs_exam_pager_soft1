const koa = require('koa')
const bodyparser = require('koa-bodyparser')
const cors = require('koa-cors')
const router = require('koa-router')()
const { Sequelize, Op } = require('sequelize')
const model = require('./model/Bolg')
//连接数据库
let sequelize = new Sequelize('blog', 'postgres', '123456', {
    host: 'hzlone.top',
    dialect: 'postgres'
})
//定义模型名称
let Bolgs = sequelize.define('blogs', model)


// sequelize.sync({ force: true }).then(() => {
//     Bolgs.bulkCreate([
//         {
//             biaoti: '程序员的厉害',
//             zhaiyao: '程序员的',
//             text: '哈哈哈',
//             type: '技术',
//             author: '哈哈哈'

//         },
//         {
//             biaoti: '程序员的厉害',
//             zhaiyao: '程序员的',
//             text: '哈哈哈',
//             type: '技术',
//             author: '哈哈哈'

//         },
//         {
//             biaoti: '程序员的厉害',
//             zhaiyao: '程序员的',
//             text: '哈哈哈',
//             type: '技术',
//             author: '哈哈哈'

//         },
//         {
//             biaoti: '程序员的厉害',
//             zhaiyao: '程序员的',
//             text: '哈哈哈',
//             type: '技术',
//             author: '哈哈哈'

//         },
//     ])

// })
//获取首页或查找数据
router.get('/cxy', async (ctx, next) => {

    let keywork = ctx.request.query.keywork || ''
    if (keywork) {
        let list= await Bolgs.findAll({
            where: {
                [Op.or]: [
                    { id: isNaN(keywork) ? 0 : parseInt(keywork) },
                    { biaoti: keywork },
                    { zhaiyao: keywork },
                    { text: keywork },
                    { type: keywork },
                    { author: keywork },
                    { createdAt:keywork  },

                ]
            }, order: ['id']
        })
        ctx.body = {
            code: 1000,
            data: list,
            msg: '查找成功'
        }
    }
    else {
        let list = await Bolgs.findAll({
            order: ['id']
        })
        ctx.body = {
            code: 1000,
            data: list,
            msg: '首页'
        }
    }
})
//获取指定id
router.get('/cxy/:id', async (ctx, next) => {
    let id = ctx.request.params.id
    let blo = await Bolgs.findByPk(id)
    if (blo) {
     Bolgs.findAll({
            where: {id:id}
        })
        ctx.body = {
            code: 1000,
            data: blo,
            msg: '成功找到id'
        }
    }
    else {
        ctx.body = {
            code: 900,
            data: '',
            msg: '查找成功'
        }
    }

})
//添加数据到数据库
router.post('/cxy',async (ctx, next) => {
    let obj = ctx.request.body;
    console.log(obj);
    console.log(111);
    await Bolgs.create(obj)
    ctx.body = obj
})
//更新数据
router.put('/cxy/:id', async (ctx, next) => {
    let obj = ctx.request.body;
    console.log(obj);
    let id = ctx.request.params.id
    let bb =await Bolgs.findByPk(id)
    if (bb) {
        Bolgs.update(obj, {
            where:{ id: id }
        })
        ctx.body = {
            code: 1000,
            data:bb,
            msg: '修改成功'
        }

    }
    else {
        ctx.body = {
            code: 900,
            data: '',
            msg: '修改失败'
        }
    }

})
//删除数据
router.delete('/cxy/:id', async (ctx, next) => {
    let id = ctx.request.params.id
    let de = await Bolgs.findByPk(id)
    if (de) {
        Bolgs.destroy({
            where: {
                id: id
            }
        })
        ctx.body = {
            code: 1000,
            data: { id },
            msg: '删除成功'
        }
    }
    else {
        ctx.body = {
            code: 900,
            data: '',
            msg: '删除失败'
        }
    }

})
let app = new koa()
app.use(cors())
app.use(bodyparser())
app.use(router.routes())
let port = 3000
app.listen(port)
console.log(`服务运行在shttp://localhost:${port}`);


