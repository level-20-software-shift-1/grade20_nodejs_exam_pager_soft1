function getlist(keywork) {
    return new Promise(function (resolve,reject) {
        $.get('http://localhost:3000/cxy?keywork'+keywork,(data)=>{
            resolve(data)
        })
        
    })
}
function getbyid(id) {
    return new Promise(function (resolve,reject) {
        $.get('http://localhost:3000/cxy/'+id,data=>{
            resolve(data)
        })
        
    })
}
function postlist(obj) {
    return new Promise(function (resolve,reject) {
        $.post('http://localhost:3000/cxy',obj,data=>{
            resolve(data)
        })
        
    })
}
function putlist(id,obj) {
    return new Promise(function (resolve,reject) {
        $.ajax({
            url:'http://localhost:3000/cxy/'+id,
            type:"put",
            data:obj,
            success:function (data) {
                resolve(data)
            }
        })
    })
}
function deletelist(id) {
    return new Promise(function (resolve,reject) {
        $.ajax({
            url:'http://localhost:3000/cxy/'+id,
            type:"delete",
            
            success:function (data) {
                resolve(data)
            }
        })
        
    })
}