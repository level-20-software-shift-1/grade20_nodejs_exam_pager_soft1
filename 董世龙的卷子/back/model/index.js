const { Sequelize,Op,DataTypes} = require('sequelize');
/* 连接数据库*/
const sequelize = new Sequelize('soft1', 'postgres', '123456', {
    host: 'www.dongshisan.top',
    dialect: 'postgres'
  });
/*利用fs找到目录下的文件进行遍历拿到请求*/
const fs =require('fs');
let file = fs.readdirSync(__dirname);
let newfile=file.filter(
    item =>{
        return item.endsWith('.js')&&item!=('index.js');
    }
)
let resultmodel={};
newfile.forEach(
    item =>{
        let modelName= item.replace('.js','');
        let model= require(__dirname+'/'+item);
        let tableName=modelName.toLowerCase();
        resultmodel[modelName]=sequelize.define(tableName,model)}
)

resultmodel.sequelize= sequelize;
resultmodel.Op=Op;
resultmodel.sync =async()=>{
    return await sequelize.sync({force:true});
}
module.exports=resultmodel;