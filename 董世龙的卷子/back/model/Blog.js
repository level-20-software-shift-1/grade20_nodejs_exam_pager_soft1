const { DataTypes }=require('sequelize');
/*建表*/
let model={
    Title:{
        type: DataTypes.STRING,
        allowNull:false
    },
    Abstract:{
        type:DataTypes.STRING,
        allowNull:false
    },
    Content:{
        type:DataTypes.STRING,
        allowNull:false
    },
    Classification:{
        type:DataTypes.STRING,
        allowNull:false
    },
    Author:{
        type:DataTypes.STRING,
        allowNull:false
    }
}
module.exports=model;