const Koa = require("koa");
const cors = require("koa-cors");
const body= require("koa-body");
const { Blog,sync }=require('./model'); 
const router=require('./router/index')
let app =new Koa();
app.use(cors()).use(body());
app.use(router)
sync().then(()=>{
    Blog.bulkCreate(
        [
            {
                Title:'刘毅',
                Abstract:'男',
                Content:'1',
                Classification:'1',
                Author:'1'
            }
        ]
    )
})

let port=8000
app.listen(port);
console.log(`服务器运行在http://localhost:${port}`);