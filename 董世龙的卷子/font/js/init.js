'use strict';
$(function () {
    getListWithKeyword();
})
function add() {
    location.href = './addOrEdit.html';
}
function getListWithKeyword(keyword) {
    keyword = keyword || ''
    if (keyword!='') {
        console.log(`你查询了${keyword}`);
    }
    
    getblogList(keyword).then(res => {
        render(res.data);
    })
}
function render(data) {
    let tb = $('#tbData');
    let rowData = $('.rowData');
    rowData.remove();
    console.log(data);
    data.forEach(item => {
        let h = `
            <tr class="rowData" key="${item.id}">
                <td>${item.id}</td>
                <td>${item.Title}</td>
                <td>${item.Abstract}</td>
                <td>${item.Content}</td>
                <td>${item.Classification}</td>
                <td>${item.Author}</td>
                <td>${item.createdAt}</td>
                <td>${item.updatedAt}</td>
                <td>
                <input type="button" value="编辑" onclick="update(${item.id})">
                <input type="button" value="删除" onclick="del(${item.id})">
                </td>
            </tr>
            `
        tb.append(h);
    })
}