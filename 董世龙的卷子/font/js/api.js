'use strict';
/查询/
function getblogList(keyword) {
    return new Promise(function (resolve, reject) {
        $.get(`${baseUrl}/blog?keyword=`+keyword, (data) => {
            resolve(data);
        })

    })
}
/id/
function getblogById(id) {
    return new Promise(function (abc, bbc) {
        $.get(`${baseUrl}/blog/${id}`, data => {
            abc(data);
        })
    })
}
/新增/
function addblog(obj) {
    return new Promise(function (resolve, reject) {
        $.post(`${baseUrl}/blog`, obj, (data) => {
            resolve(data);
        })
    })
}
/更新/
function updateblog(id, obj) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `${baseUrl}/blog/${id}`,
            type: "PUT",
            data: obj,
            success: function (data) {
                resolve(data);
            }
        });
    })
}
/删除/
function delblogById(id) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `${baseUrl}/blog/${id}`,
            type: "delete",
            success: function (data) {
                resolve(data);
            }
        });
    })
}
