'use strict'
$(function(){
    console.log('加载成功');
    getQuery();
})

function readList(data){
    let tb = $('#taData');
    let row = $('.rowData');
    row.move();
    data.forEach(item=>{
        let html = `
      <tr class="rowData" key="${item.id}">
      <td>${item.id}</td>
      <td>${item.title}</td>
      <td>${item.abstract}</td>
      <td>${item.content}</td>
      <td>${item.classify}</td>
      <td>${item.author}</td>
      <td>${item. time}</td>
      <td>
          <input type="button" value="编辑" onclick="update(${item.id})">
          <input type="button" value="删除" onclick="dele(${item.id})">
      </td>    
     </tr>
        `
        tb.append(html);
    })
}

function getQuery(){
    keyword = keyword || '';
    getListQuery(keyword).then(res=>{
            readList(res.data);
    })
}


function update(id){
    window.location.href='./addOrEdit.html?id'+id;
}

function dele(){
    let confirmDel = confirm(`确定删除该ID吗？`);
    if(confirmDel){
        getListDelete(id).then(res=>{
            if(data.code === 1000){
                let id = res.data.id;
                $[`[key=${id}]`].remove();
            }else{
                alert(data.msg);
            }
        })
    }
}