'use strict'
//查询和显示列表
function getListQuery(keyword){
    return new Promise(function(res,rej){
        $.get('http://localhost:3000/product?keyword='+keyword,(data)=>{
            res(data);
        })
    })
}

//获取ID
function getListId(id){
    return new Promise(function(res,rej){
        $.get(`http://localhost:3000/product/${id}`,(data)=>{
            res(data);
        })
    })
}

//添加
function getListAdd(obj){
    return new Promise(function(res,rej){
        $.get('http://localhost:3000/product',obj,(data)=>{
            res(data);
        })
    })
}

//编辑
function getListUpdate(id,obj){
    return new Promise(function(res,rej){
        $.ajax(
            {
                url:`http://localhost:3000/product/${id}`,
                data:obj,
                type:'put',
                success:data=>{
                    res(data);
                }
            }
        )
    })
}

//删除
function getListDelete(id){
    return new Promise(function(res,rej){
        $.ajax(
            {
                url:`http://localhost:3000/product/${id}`,
                type:'delete',
                success:data=>{
                    res(data);
                }
            }
        )
    })
}