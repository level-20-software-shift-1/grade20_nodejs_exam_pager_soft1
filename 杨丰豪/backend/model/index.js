'use strict'
const fs = require('fs');
const { Sequelize, DataTypes, Op } = require('sequelize');

const sequelize = new Sequelize('blog', 'postgres', '123456', {
    host: 'host.flymi.top',
    dialect: 'postgres'
})

let files = fs.readdirSync(__dirname);
let readFiles = files.filter(item => {
    return item.endsWith('.js') && item !== 'index.js';
})

let result = {};

readFiles.forEach(item => {
    let modelName = item.replace('.js', '');
    let lowName = modelName.toLowerCase();
    let model = require(__dirname + '/' + item);
    result[modelName] = sequelize.define(lowName,model);
})

result.Op = Op;
result.sequelize = sequelize;

result.sync = async()=>{
    return await sequelize.sync({force:true});
}

module.exports = result;
