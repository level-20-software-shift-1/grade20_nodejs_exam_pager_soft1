'use strict'
const { DataTypes } = require('sequelize');

let model={
    //标题
    title:{
        type:DataTypes.STRING,
        allowNull:false,
    },
    //摘要
    abstract:{
        type:DataTypes.STRING,
        allowNull:false,
    },
    //内容
    content:{
        type:DataTypes.STRING,
        allowNull:false,
    },
    //分类
    classify:{
        type:DataTypes.STRING,
        allowNull:false,
    },
    //作者
    author:{
        type:DataTypes.STRING,
        allowNull:false,
    },
    //时间
    time:{
        type:DataTypes.STRING,
        allowNull:false,
    }
}

module.exports = model;