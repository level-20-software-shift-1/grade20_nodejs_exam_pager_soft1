'use strict'
const koa = require('koa');
const cors = require('koa-cors');
const Router = require('./router/index');
const {Products,sync} = require('./model');

let bodyParser = require('koa-bodyparser');

sync().then(()=>{
    Products.bulkCreate([
        {
            title:'今天吃什么？',
            abstract:'今天吃什么？',
            content:'今天吃面',
            classify:'生活',
            author:'东东',
            time:'2022-04-08'
        }
    ])
})

let app = new koa();

app.use(cors());
app.use(Router());
app.use(bodyParser());

let port = 3000;

app.listen(port);

console.log(`服务器地址：http://localhost:${port}`);