'use strict'
//初始化工具
const koa = require('koa');
const router = require('koa-router')();
const cors = require('koa-cors');
const bodyparser = require('koa-bodyparser');
const { Sequelize, DataTypes, Op } = require('sequelize');

let app = new koa();
//连接云数据库
const sequelize = new Sequelize('biaoti', 'postgres', 'as123456', {
    host: 'host.wgs.life',
    dialect: 'postgres',
});

//同步数据
const student = require('./model/Student');
let Students = sequelize.define('students', student);
// sequelize.sync({force:true}).then(() => {
//     Students.bulkCreate([
//         {
//             biaoti:'小a',
//             zaiyao:'小a',
//             neirong:'小a',
//             fenglei:'小a',
//             zuozhe:'小a'
//         },
//     ])
// });
//解决跨域问题
app.use(cors());
app.use(bodyparser());
//用异步函数解决查找问题
router.get('/student', async (ctx, next) => {
    let keyword = ctx.request.query.keyword || '';
    if (keyword) {
        let a = await Students.findAll({
            where: {
                [Op.or]: [
                    { biaoti: keyword },
                    { zaiyao: keyword },
                    { neirong: keyword },
                    { fenglei: keyword },
                    { zuozhe: keyword },
                ]
            },
            order: ['id']
        });
        ctx.body = {
            code: 1000,
            data: a,
            msg: '请求成功！'
        }
    } else {
        let b = await Students.findAll({
            order: ['id']
        });
        // console.log(a);
        ctx.body = {
            code: 900,
            data: b,
            msg: '请求失败a！快去查找'
        }
    }
})

router.get('/student/:id', async (ctx, next) => {
    let id = ctx.request.params.id;
    console.log(id);
    let a = await Students.findByPk(id);
    if (a) {
        ctx.body = {
            code: 1000,
            data: a,
            msg: '请求成功！'
        }
    } else {
        ctx.body = {
            code: 900,
            data: '',
            msg: '请求失败！'
        }
    }
})
router.post('/student', async (ctx, next) => {
    let ob = ctx.request.body;
    console.log(ob);
    await Students.create(ob);
    ctx.body = ob;
})
//用异步函数解决修改问题
router.put('/student/:id', async (ctx, next) => {
    console.log(5);
    let id = ctx.request.params.id;
    let obj = ctx.request.body;
    console.log(id);
    console.log(obj);
    let a = await Students.findByPk(id);
    if (a) {

        console.log(90);
        Students.update(obj, {
            where: {
                id: id
            }
        })
        ctx.body = {
            code: 1000,
            data: a,
            msg: "修改成功！"
        }
    } else {
        ctx.body = {
            code: 900,
            data: '',
            msg: "修改失败！"
        }
    }
})

//用异步函数解决删除问题
router.delete('/student/:id', async (ctx, next) => {
    let id = ctx.request.params.id;
    // console.log(id);
    let a = await Students.findByPk(id);
    if (a) {
        Students.destroy({
            where: {
                id: id
            }
        })
        ctx.body = {
            code: 1000,
            data: { id },
            msg: '删除成功~'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '你要删除的对象不存在，请重试~'
        }
    }
})


app.use(router.routes());

let port = 5000;

app.listen(port);

console.log(`服务器地址运行在:http://localhost:${port}`);