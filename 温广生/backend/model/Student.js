'use strict';

const { DataTypes } = require('sequelize');

let model = {
    ///我这里全部用string定义模型
    biaoti: {
        type: DataTypes.STRING,
        allowNull: false
    },
    zaiyao: {
        type: DataTypes.STRING,
        allowNull: false
    },
    neirong: {
        type: DataTypes.STRING,
        allowNull: false
    },
    fenglei: {
        type: DataTypes.STRING,
        allowNull: false
    },
    zuozhe: {
        type: DataTypes.STRING,
        allowNull: false
    },

}
module.exports = model;