'use strict'

function aslist(keyword) {
    return new Promise(function (params, reject) {
        $.get(`${baseUrl}/Student?keyword=` + keyword, (date) => {
            params(date);
        })
    })
}

function asByid(id) {
    return new Promise(function (params, reject) {
        $.get(`${baseUrl}/Student/${id}`, date => {
            params(date);
        })
    })
}

function asadd(obj) {
    return new Promise(function (resolve, reject) {
        $.post(`${baseUrl}/Student`, obj, (data) => {
            resolve(data);
        })
    })
}

function asupdate(id, obj) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `${baseUrl}/Student/${id}`,
            type: "PUT",
            data: obj,
            success: function (data) {
                // 请求成功后的回调函数
                resolve(data);
            }
        })
    })
}

function asdelete(id) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `${baseUrl}/Student/${id}`,
            type: "delete",
            success: function (data) {
                // 请求成功后的回调函数
                resolve(data);
            }
        })
    })
}