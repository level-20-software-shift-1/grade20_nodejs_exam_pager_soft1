'use strict'

const { Sequelize , Op, Model } = require('sequelize');
const fs = require('fs');
// postgres数据库连接
let sequelize = new Sequelize("usefortest" , "postgres" , "123456" , {
    host : "mlssq.xyz",
    dialect : "postgres"
})
// 获取当前文件夹的表文件
let file = fs.readdirSync(__dirname);
let getfile = file.filter(item=>{
    return item.endsWith(".js") && item !== 'index.js'
});
// 数据中表创建
let result = {};
getfile.forEach(item=>{
    let modelname = item.replace(".js" , "");
    let model = require(__dirname + '/' + modelname);
    result[modelname] = sequelize.define(modelname.toLocaleLowerCase(),model);
})
// 挂载
result.sequelize = sequelize;
result.Op = Op;
// 同步
result.sync = async function(){
    return await sequelize.sync({force : true});
}

module.exports = result;