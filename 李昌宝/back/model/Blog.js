'use strice'
// 表创建
const { DataTypes } = require('sequelize');

let table = {
    title : {
        type : DataTypes.STRING,
        allowNull : false
    },
    abstract : {
        type : DataTypes.STRING,
        allowNull : false
    },
    content : {
        type : DataTypes.STRING,
        allowNull : false
    },
    classify : {
        type : DataTypes.STRING,
        allowNull : false
    },
    author : {
        type : DataTypes.STRING,
        allowNull : false
    },
    time : {
        type : DataTypes.TIME,
        allowNull : false
    }
}

module.exports = table;