'use strict'

const Koa = require('koa');
const bodyparser = require('koa-bodyparser');
const cors = require('koa-cors');

const router = require('./router/index');
// 解构赋值获取模块 方法
const { Blog , sync } = require('./model/index');

let app = new Koa();

// 同步
sync();

app.use(cors());
app.use(bodyparser());
app.use(router());

let port = 3000;
app.listen(port);
console.log("http://localhost:3000")