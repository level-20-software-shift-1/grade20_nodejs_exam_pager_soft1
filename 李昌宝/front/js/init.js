'use struct'
// 渲染函数 获取数据
function render(data){
    let tbdata = $('#tbData');
    let tbrow = $('.tbRow');
    tbrow.remove();
    data.forEach(element => {
        let h = `
            <tr class="tbRow" key=${element.id}>
                <td>${element.id}</td>
                <td>${element.title}</td>
                <td>${element.abstract}</td>
                <td>${element.content}</td>
                <td>${element.classify}</td>
                <td>${element.author}</td>
                <td>${element.time}</td>
                <td>
                    <input type="button" value="编辑" onclick="btnEdit(${element.id})">
                    <input type="button" value="删除" onclick="btnDel(${element.id})">
                </td>
            </tr>
        `
        tbdata.append(h)
    });
}

function getListWithKeyword(keyword){
    keyword = keyword || '';
    getProductList(keyword).then(res=>{
        render(res.data);
    })
}
// 匿名函数，表示进入这个文件就 执行
$(function(){
    getListWithKeyword();
})