'use strict'
// 前端主页数据
function getProductList(keyword){
    return new Promise(function(resolve,reject){
        $.get(`${baseUrl}/product?keyword=` + keyword , data=>{
            resolve(data);
        })
    })
}
// 编辑页获取 数据
function getProductListById(id){
    return new Promise(function(resolve,reject){
        $.get(`${baseUrl}/product/${id}` , data=>{
            resolve(data);
        })
    })
}
// 增加商品
function addProduct(id){
    return new Promise(function(resolve,reject){
        $.post(`${baseUrl}/product/${id}` , data=>{
            resolve(data);
        })
    })
}
// 修改
function updateProduct(id,obj){
    return new Promise(function(resolve,reject){
        $.ajax({
            url : `${baseUrl}/product/${id}`,
            data : obj,
            type : "put",
            success:function(data){
                resolve(data);
            }
        })
    })
}
// 删除
function delProduct(id){
    return new Promise(function(resolve,reject){
        $.ajax({
            url : `${baseUrl}/product/${id}`,
            type : "delete",
            success : function(data){
                resolve(data);
            }            
        })
    })
}