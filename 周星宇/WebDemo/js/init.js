'use strict'

$(function () {
    getListWithKeyword();
})
function add() {
    location.href = './addOrEdit.html'
}

function getListWithKeyword(keyword) {
    keyword = keyword || ''
    getBlogList(keyword).then(res => {
        render(res.data);
    })
}

function render(data) {
    let tb = $('#tbData');
        let rowData = $('.rowData');
        rowData.remove();
        data.forEach(item => {
        let h=`
        <table class="rowData" name="${item.id}">
        <tr>
            <td>${item.id}</td>
            <td>${item.headLine}</td>
            <td>${item.abstract}</td>
            <td>${item.content}</td>
            <td>${item.classify}</td>
            <td>${item.author}</td>
            <td>${item.postedTimeg}</td>
            <td>
                <input type="button" value="编辑" onclick="update(${item.id})">
                <input type="button" value="删除" onclick="del(${item.id})">
            </td>
        </tr>
    </table>
            `
            tb.append(h);
        });
}