'use strict'

function getBlogList(keyword) {
    return new Promise(function (resolve, reject) {
        $.get(`${baseUrl}/blog?keyword=`+keyword, (data) => {
            resolve(data);
        })
    })
}

// 根据id获取
function getBlogById(id) {
    return new Promise(function (abc, bbc) {
        $.get(`${baseUrl}/blog/${id}`, data => {
            abc(data);
        })
    })
}

// 新增
function addBlog(obj) {
    return new Promise(function (resolve, reject) {
        $.post(`${baseUrl}/blog`, obj, (data) => {
            resolve(data);
        })
    })
}

// 更新
function updateBlog(id, obj) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `${baseUrl}/blog/${id}`,
            type: "PUT",
            data: obj,
            success: function (data) {

                resolve(data);
            }
        });
    })
}
// 删除
function delBlogById(id) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `${baseUrl}/blog/${id}`,
            type: "delete",
            success: function (data) {
                resolve(data);
            }
        });
    })
}