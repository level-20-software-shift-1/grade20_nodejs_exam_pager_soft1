'use strict'

const { DataTypes }=require('sequelize');

let model={
    headLine:{
        type:DataTypes.STRING,
        allowNull:false
    },
    abstract:{
        type:DataTypes.STRING,
        allowNull:false
    },
    content:{
        type:DataTypes.STRING,
        allowNull:false
    },
    classify:{
        type:DataTypes.STRING,
        allowNull:false
    },
    author:{
        type:DataTypes.STRING,
        allowNull:false
    },
    postedTimeg:{
        type:DataTypes.TIME,
        allowNull:false
    },
}

module.exports=model;