'use strict'

const Koa=require("koa");
const router=require('koa-router')();
const bodyParser=require('koa-bodyparser');
const cors=require('koa-cors');
const Sequelize=require('sequelize');
const blog=require('./model/Blogs');
const { Op } = require('sequelize');


let app =new Koa();

let sequelize=new Sequelize('test','postgres','123456',{
    dialect:'postgres',
    host:'allmyu.fun'
})

let Blogs=sequelize.define('blogs',blog);

sequelize.sync({force:true}).then(()=>{
    Blogs.bulkCreate([
        {
            id:1,
            headLine:'怎么这么难',
            abstract:'难顶',
            content:'论考试不好写',
            classify:'考试',
            author:'我',
            postedTimeg:'2022-04-08 19:50'
        },
        {
            id:2,
            headLine:'你好',
            abstract:'怎么',
            content:'运行',
            classify:'不起来',
            author:'我',
            postedTimeg:'2022-04-08 19:50'
        },
    ])
})


router.get('/blog',async(ctx,next)=>{
    let keyword = ctx.request.query.keyword || '';
    if (keyword) {
        let list;
        list=await Blogs.findAll({
            where:{
                [Op.or]:[
                    {id:isNaN(keyword)?0:parseInt(keyword)},
                    {headLine:keyword},
                    {abstract:keyword},
                    {content:keyword},
                    {classify:keyword},
                    {author:keyword},
                    {postedTimeg:keyword}
                ]
            },
            order:['id']
        })
        ctx.body = {
            code: 1000,
            data: list,
            msg: '请求成功'
        }
    } else {
        let list=await Blogs.findAll({
            order:['id']
        })
        ctx.body = {
            code: 1000,
            data: list,
            msg: '请求成功'
        }
    }
})

router.get('/blog/:id',async(ctx,next)=>{
    let id = ctx.request.params.id;
    let blog =await Blogs.findByPk(id);
    if (blog) {
        ctx.body = {
            code: 1000,
            data: blog,
            msg: '获取成功'
        }
    } else {
        ctx.body = {
            code: 900,
            data: '',
            msg: '找不到'
        }
    }

})

router.post('/blog',async(ctx,next)=>{
    let obj = ctx.request.body;
    await Blogs.create(obj);
    ctx.body = obj;
})

router.put('/blog/:id',async(ctx,next)=>{
    let id = ctx.request.params.id;
    let blog=await blog.findByPk(id)
    if(blog){
        blog(obj,{
            id:id
        })
        ctx.body={
            code:1000,
            data:blog,
            msg:'成功'
        }
    }else{
        ctx.body={
            code:900,
            data:'',
            msg:'失败'
        }
    }
})

router.delete('/blog/:id',async(ctx,next)=>{
    let id = ctx.request.params.id;
    let blog = await Blogs.findByPk(id);
    if(blog){
        Blogs.destroy({
            where:{
                id:id
            }
        })
        ctx.body={
            code:1000,
            data:{id},
            msg:'删除成功'
        }
    }else{
        ctx.body={
            code:400,
            data:null,
            msg:'删除失败'
        }
    }
})

app.use(cors());
app.use(bodyParser());
app.use(router.routes());

let port=3000;
app.listen(port);
console.log(`运行在:http://localhost:${port}`);