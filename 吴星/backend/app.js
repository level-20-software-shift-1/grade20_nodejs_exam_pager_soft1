'use strict';

const Koa = require('koa');
const router = require('koa-router')();
const bodyParser = require('koa-bodyparser');
const cors = require('koa-cors');
const Sequelize = require('sequelize');
const { stockNum, supplier } = require('./model/boke');

let app = new Koa();


let sequelize = new Sequelize('soft3db', 'postgres', 'Wx3339128080', {
    host: 'www.zgxhh.top',
    dialect: 'postgres'
})

//引入数据库模型
const boke = require('./model/Boke');

//定义应用模型
let bokes = sequelize.define('bokes', boke);

//插入数据
// sequelize.sync({ force: true }).then(() => {
//     bokes.bulkCreate([
//         {
//             // id: 1,
//             biaoti: '论EF Cor的自我修养',
//             zaiyao: '论EF Cor的自我修养',
//             neiron: '论EF Cor的自我修养',
//             fenlei:'.Net',
//             zuoze:'inCerry',
//             shijian:'默认时间'
//         },
//         {
//             // id: 2,
//             biaoti: '论EF Cor的自我修养',
//             zaiyao: '论EF Cor的自我修养',
//             neiron: '论EF Cor的自我修养',
//             fenlei:'.Net',
//             zuoze:'inCerry',
//             shijian:'默认时间'
//         },
//         {
//             // id: 3,
//             biaoti: '论EF Cor的自我修养',
//             zaiyao: '论EF Cor的自我修养',
//             neiron: '论EF Cor的自我修养',
//             fenlei:'.Net',
//             zuoze:'inCerry',
//             shijian:'默认时间'
//         },
//         {
//             // id: 4,
//             biaoti: '论EF Cor的自我修养',
//             zaiyao: '论EF Cor的自我修养',
//             neiron: '论EF Cor的自我修养',
//             fenlei:'.Net',
//             zuoze:'inCerry',
//             shijian:'默认时间'
//         },
//         {
//             id: 5,
//             biaoti: '论EF Cor的自我修养',
//             zaiyao: '论EF Cor的自我修养',
//             neiron: '论EF Cor的自我修养',
//             fenlei:'.Net',
//             zuoze:'inCerry',
//             shijian:'默认时间'
//         }
//     ])
// });


router.get('/boke', async (ctx, next) => {
    let keyword = ctx.request.query.keyword || '';

    if (keyword) {
        let list = await bokes.findAll({
            where: {
                [Op.or]: [
                    { id: id },
                    { biaoti: keyword },
                    { zaiyao: keyword },
                    { neiron: keyword },
                    { fenlei: keyword },
                    { zuoze: keyword }
                ]
            }
        })
        ctx.body = {
            code: 1000,
            data: list,
            msg: '查找成功'
        }
    } else {
        let list = await bokes.findAll({
            order: ['id']
        })
        ctx.body = {
            code: 1000,
            data: list,
            msg: '展示'
        }
    }
})

//根据id找到商品
router.get('/boke/:id', async (ctx, next) => {

    let id = ctx.request.params.id
    let boke = await bokes.findByPk(id);

    if (boke) {
        ctx.body = {
            code: 1000,
            data: boke,
            msg: 'id有对应的商品'
        }
    } else {
        ctx.body = {
            code: 900,
            data: '',
            msg: '商品不存在'
        }
    }
})


router.post('/boke', async (ctx, next) => {

    let obj = ctx.request.body;
    await bokes.create(obj);
    ctx.body = obj;
})


router.put('/boke', async (ctx, next) => {
    let id = ctx.request.params.id;
    let obj = ctx.request.body;
    let boke = await bokes.findByPk(id);

    if (boke) {
        bokes.update(obj, {
            where: {
                id: id
            }
        })
        ctx.body = {
            code: 1000,
            data: boke,//返回修改成功后的对象
            msg: '修改成功'
        }
    } else {
        ctx.body = {
            code: 900,
            data: '',
            msg: '修改的商品不存在'
        }
    }
})


router.delete('/boke', async (ctx, next) => {
    console.log('删除一下');
    let id = ctx.request.params.id;
    let boke = await bokes.findByPk(id);

    if (boke) {
        // bokes=arr;

        bokes.destroy({
            where: {
                id: id
            }//根据id进行摧毁
        })

        ctx.body = {
            code: 1000,
            data: { id },
            msg: '删除成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '要删除的商品不存在'
        }
    }
})


app.use(cors());
app.use(bodyParser());
app.use(router.routes());



let port = 3000;
app.listen(port);

console.log(`服务器地址运行在http://localhost:${port}`);