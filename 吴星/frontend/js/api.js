'use strict'

//获取商品列表
function getbokeList(keyword){
    return new Promise(function(resolve,reject){
        $.get(`${baseUrl}/boke?keyword=`+keyword,(data)=>{
            resolve(data);
            //接收返回的data

        })
    })
}

//根据Id获得指定商品
function getbokeByid(id){
    return new Promise(function(resolve,reject){
        $.get(`${baseUrl}/boke/${id}`,data=>{
            resolve(data);
        })
    })
}

//新增商品
function addboke(obj){
    return new Promise(function(resolve,reject){
        $.post(`${baseUrl}/boke/`,obj,(data)=>{
        console.log(data);   
        resolve(data);
        })
    })
}

//更新商品
function updateboke(id,obj){
    return new Promise(function(resolve,reject){
        $.ajax({
            url:`${baseUrl}/boke/${id}`,
            type:"PuT",
            data:obj,
            success:function(data){
               //请求成功后的回调函数
                resolve(data);
            }
        });
            
    })
}

//删除商品

function delbokeByid(id){
    return new  Promise(function(resolve,reject){
        $.ajax({
            url:`${baseUrl}/boke/${id}`,
            type:"delete",
            success:function(data){
                resolve(data);
            }
        })
    })
}

