'use strict'

const { DataTypes } = require('sequelize');

let models = {
    title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    digest:{
        type:DataTypes.STRING,
        allowNull:false
    },
    content:{
        type:DataTypes.STRING,
        allowNull:false
    },
    classify:{
        type:DataTypes.STRING,
        allowNull:false
    },
    author:{
        type:DataTypes.STRING,
        allowNull:false
    }
}
module.exports = models;