'use strict'

const koa = require('koa');
const bodyparser = require('koa-bodyparser');
const cors = require('koa-cors')
const routerfs = require('./router/index')
const {Blogs,sync}= require('./model');

// sync().then(()=>{
//   Blogs.bulkCreate([
//     {
//         id: 1,
//         title: 'DDD之我见',
//         digest: 'DDD之我见',
//         content: 'DDD之我见',
//         classify: '编程技术',
//         author:'某大神'
//     },
//     {
//         id: 2,
//         title: '论core的自我修养',
//         digest: '论core的自我修养',
//         content: '论core的自我修养',
//         classify: '.NET',
//         author:'某大神'
//     },
//     {
//         id: 3,
//         title: '大数据',
//         digest: '大数据',
//         content: '大数据',
//         classify: 'Linux',
//         author:'居家博士'
//     },
//   ])
// });

let app = new koa();
app.use(cors());
app.use(bodyparser());
app.use(routerfs());

let port = 1228;
app.listen(port);
console.log(`本次服务器将运行在：http://localhost:${port}`);