'use strict'

'use strict';
let { Blogs, Op } = require('../model');

// let blog = [
//     {
//         id: 1,
//         title: 'DDD之我见',
//         digest: 'DDD之我见',
//         content: 'DDD之我见',
//         classify: '编程技术',
//         author:'某大神'
//     },
//     {
//         id: 2,
//         title: '论core的自我修养',
//         digest: '论core的自我修养',
//         content: '论core的自我修养',
//         classify: '.NET',
//         author:'某大神'
//     },
//     {
//         id: 3,
//         title: '大数据',
//         digest: '大数据',
//         content: '大数据',
//         classify: 'Linux',
//         author:'居家博士'
//     },

// ]

let fun_listed = async (ctx, next) => {
    let keyword = ctx.request.query.keyword || '';
    if (keyword) {
       let list = await Blogs.findAll({
            where: {
                [Op.or]: 
                [
                    { id: isNaN(keyword) ? 0 : parseInt(keyword) },
                    { title: keyword },
                    { digest: keyword },
                    { content: keyword},
                    { classify: keyword },
                    { author: keyword },
                ]
            },
            order:['id']
        })
        ctx.body = {
            code: 1000,
            data: list,
            msg: '成功'
        }
    } else {
        let list = await Blogs.findAll({
            order:['id']
        });
        ctx.body = {
            code: 1000,
            data: list,
            msg: '成功'
        }
    }
}
let fun_getbyided = async (ctx, next) => {
    let id = ctx.request.params.id;
    let blog = await Blogs.findByPk(id);
    if (blog) {
        ctx.body = {
            code: 1000,
            data: blog,
            msg: '获取成功'
        }
    } else {
        ctx.body = {
            code: 500,
            data: '',
            msg: '您查找的数据不存在'
        }
    }
}
let fun_posted =async (ctx, next) => {
    let obj = ctx.request.body;

    await Blogs.create(obj);
    ctx.body = obj;
}
let fun_puted =async (ctx, next) => {
    let id = ctx.request.params.id;
    let obj = ctx.request.body;
    let blog = await Blogs.findByPk(id);
    
    if (blog) {
        Blogs.update(obj,{
            where:{
                id:id
            }
        })
        ctx.body = {
            code: 1000,
            data: blog,
            msg: '修改成功'
        }
    } else {
        ctx.body = {
            code: 600,
            data: '',
            msg: '您修改的数据不存在'
        }
    }
}
let fun_deleted =async (ctx, next) => {

    let id = ctx.request.params.id;
    let blog = await Blogs.findByPk(id);
    if (blog) {
        Blogs.destroy({
            where:{
                id:id
            }
        })
        ctx.body = {
            code: 1000,
            data: { id },
            msg: '删除成功'
        }
    } else {
        ctx.body = {
            code: 300,
            data: null,
            msg: '删除记录不存在！'
        }
    }
}


module.exports = {
    'get /blog': fun_listed,
    'get /blog/:id': fun_getbyided,
    'post /blog': fun_posted,
    'put /blog/:id': fun_puted,
    'delete /blog/:id': fun_deleted
}