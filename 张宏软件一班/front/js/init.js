'use strict';

$(function () {
    getKeyword();
})

function getKeyword(keyword) {
    keyword = keyword || ''
    getList(keyword).then(res => {
        console.log(res);
        render(res.data);
    })
}

function render(data) {
    let tb = $('#tbData');
    let rowData = $('.rowData');
    rowData.remove();
    data.forEach(item => {
        let h = `
            <tr class="rowData" key="${item.id}">
                <td>${item.id}</td>
                <td>${item.title}</td>
                <td>${item.digest}</td>
                <td>${item.content}</td>
                <td>${item.classify}</td>
                <td>${item.author}</td>
                <td>
                <input type="button" value="编辑" onclick="update(${item.id})">
                <input type="button" value="删除" onclick="del(${item.id})">
                </td>
            </tr>
            `
        tb.append(h);
    })
}