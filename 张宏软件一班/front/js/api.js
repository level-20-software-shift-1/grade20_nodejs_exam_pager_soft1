'use strict';

function getList(keyword) {
    return new Promise(function (resolve, reject) {
        $.get(`${configUrl}/blog?keyword=`+keyword, (data) => {
            resolve(data);
        })
    })
}

function getById(id) {
    return new Promise(function (resolve, reject) {
        $.get(`${configUrl}/blog/${id}`, data => {
            resolve(data);
        })
    })
}

function addBy(obj) {
    return new Promise(function (lll, kkk) {
        $.post(`${configUrl}/blog`, obj, (data) => {
            lll(data);
        })
    })
}
function update(id, obj) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `${configUrl}/blog/${id}`,
            type: "PUT",
            data: obj,
            success: function (data) {
                resolve(data);
            }
        });
    })
}

function delById(id) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `${configUrl}/blog/${id}`,
            type: "delete",
            success: function (data) {
                resolve(data);
            }
        });
    })
}
