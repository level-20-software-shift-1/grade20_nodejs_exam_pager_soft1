'use strict';

const { DataTypes } = require('sequelize');

let model = {
    IDBCursor: {
        type: DataTypes.STRING,
        allowNull: false
    },
    headline: {
        type: DataTypes.STRING,
        allowNull: false
    },
    abstract: {
        type: DataTypes.STRING,
        allowNull: false
    },
    cont: {
        type: DataTypes.STRING,
        allowNull: false
    },
    class: {
        type: DataTypes.STRING,
        allowNull: false
    },
    author: {
        type: DataTypes.INET,
        allowNull: false
    },
    posted: {
        type: DataTypes.STRING,
        allowNull: false
    },
}
module.exports = model;