'use strict';

const { Sequelize, DataTypes,Op } = require('sequelize');
const {db_dialect,db_host,db_username,db_password,db_database}=require('../config/db');
const fs = require('fs');

const sequelize = new Sequelize(db_database, db_username, db_password, {
    host: db_host,
    dialect: db_dialect
});

let files = fs.readdirSync(__dirname);
let resfiles = files.filter(item => {
    return item.endsWith('.js') && item !== 'index.js';
})

let resultModel = {};

resfiles.forEach(item => {
    let modelName = item.replace('.js', '');
    console.log(modelName);
    let model = require(__dirname + '/' + item);
    let tableName=modelName.toLowerCase();
    resultModel[modelName] = sequelize.define(tableName, model);
})
resultModel.sequelize = sequelize;
resultModel.Op = Op;

resultModel.sync = async () => {
    return await sequelize.sync({ force: true });
}

module.exports = resultModel

