'use strict';

const Koa = require('koa');
const cors = require('koa-cors');
const bodyParser = require('koa-bodyparser');
const routerMiddleware = require('./router/index');

const { Products, sync } = require('./model');
sync().then(() => {
  Products.bulkCreate([
        {
            id: '1',
            headline: '论EF Core的自我修养',
            abstract: '论EF Core的自我修养',
            cont: '论EF Core的自我修养',
            class: '.Net',
            author : 'InCerry',
            posted : '2022-04-06 08:47'
        },
        {
            id: 'DDD之我见',
            headline: 'DDD之我见',
            abstract: 'DDD之我见',
            cont: '编辑技术',
            class: '某大神',
            author : '2022-04-03',
            posted : '23：47'
        },
        {
            id: 'nginx负载平衡的几种策略',
            headline: 'nginx负载平衡的几种策略',
            abstract: 'nginx负载平衡的几种策略',
            cont: '服务器',
            class: '老胡来也',
            author : '2022-04-06',
            posted : '08：47'
        },
        {
            id: 'Linux用户创建的学习研究',
            headline: 'Linux用户创建的学习研究',
            abstract: 'Linux用户创建的学习研究',
            cont: 'Linux',
            class: '某大神',
            author : '2022-04-06',
            posted : '08：47'
        },
        {
            id: '大数据仪表盘探讨',
            headline: '大数据仪表盘探讨',
            abstract: '大数据仪表盘探讨',
            cont: '大数据',
            class: '居家博士',
            author : '2022-04-18',
            posted : '16：18'
        },

      
    ])
});
let app = new Koa();

app.use(cors());
app.use(bodyParser());
app.use(routerMiddleware());

let port = 3000;

app.listen(port);

console.log(`服务器运行在如下地址：http://localhost:${port}`);