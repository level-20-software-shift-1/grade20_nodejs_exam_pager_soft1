'use strict';

$(function () {
    getListWithKeyword();
})

function getListWithKeyword(keyword) {
    keyword = keyword || ''
    // console.log(keyword);
    getProductList(keyword).then(res => {
        console.log(res);
        render(res.data);
    })
}

function render(data) {
    let tb = $('#tbData');
    let rowData = $('.rowData');
    rowData.remove();
    data.forEach(item => {
        let h = `
            <tr class="rowData" key="${item.id}">
                <td>${item. IDBCursor}</td>
                <td>${item.headline}</td>
                <td>${item.abstract}</td>
                <td>${item.cont}</td>
                <td>${item. class}</td>
                <td>${item. author}</td>
                <td>${item.  posted}</td>
                <td>
                <input type="button" value="编辑" onclick="update(${item.id})">
                <input type="button" value="删除" onclick="del(${item.id})">
                </td>
            </tr>
            `
        tb.append(h);
    })
}