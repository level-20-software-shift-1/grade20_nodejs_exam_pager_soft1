'use strict'

const fs = require('fs');
const { DataTypes, Sequelize, Op } = require('sequelize');

let obj = {};
let sequelize = new Sequelize('nodejsdemo', 'postgres', 'yangfan4825648', {
    host: 'pigfarm.top',
    dialect: 'postgres',
});

obj.Blog = sequelize.define('blog', {
    title: {
        type: DataTypes.STRING,
    },
    digest: {
        type: DataTypes.STRING,
    },
    text: {
        type: DataTypes.STRING,
    },
    classify: {
        type: DataTypes.STRING,
    },
    author: {
        type: DataTypes.STRING,
    },
})

obj.Op = Op;
obj.sequelize = sequelize;
obj.sync = async () => {
    return await sequelize.sync({ force: true });
}

module.exports = obj;