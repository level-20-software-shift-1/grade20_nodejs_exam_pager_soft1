'use strict'

function getProdcutList(keyword){
    return new Promise(function(resolve,reject){
        $.get(`${baseUrl}/product?keyword=${keyword}`,res=>{
            resolve(res)
        })
    })   
}
function getListById(id){
    return new Promise(function(resolve,reject){
        $.get(`${baseUrl}/product/${id}`,res=>{
            resolve(res)
        })
    })   
}
function addProdcut(obj){
    return new Promise(function(resolve,reject){
        $.post(`${baseUrl}/product`,obj,res=>{
            resolve(res)
        })
    })   
}
function updateProdcut(id,obj){
    return new Promise(function(resolve,reject){
        $.ajax({
            url:`${baseUrl}/product/${id}`,
            type:'put',
            data:obj,
            success:function(res){
                resolve(res)
            }
        })
    })   
}
function delProdcut(id){
    return new Promise(function(resolve,reject){
        $.ajax({
            url:`${baseUrl}/product/${id}`,
            type:'delete',
            data:'',
            success:function(res){
                resolve(res)
            }
        })
    })   
}