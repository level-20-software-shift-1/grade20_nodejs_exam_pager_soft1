'use strict'

// 添加
function Insert() {
    window.location.href = `./addOrEdit.html`;
}

// 修改
function Update(id) {
    window.location.href = `./addOrEdit.html?id=${id}`;
}

// 删除
function Delete(id) {
    $.ajax({
        url: `http://localhost:3000/${id}`,
        type: 'DELETE',
        success: (res) => {
            alert(res);
            window.location.href = `./index.html`;
        }
    })
}

// 查找
function Query() {
    let keyword = $('[id = keyword]').val();
    let tb = $('table');
    tb.html(`
        <tr>
            <input type="text" placeholder="请输入查找内容、关键字" id="keyword">
            <input type="button" value="搜索" onclick="Query()">
            <input type="button" value="添加" onclick="Insert()">
        </tr>
        <tr>
            <td>ID</td>
            <td>标题</td>
            <td>摘要</td>
            <td>内容</td>
            <td>分类</td>
            <td>作者</td>
            <td>发表时间</td>
            <td>操作</td>
        </tr>
    `)
function render(res){
    $('.rowdata').remove()
    res.forEach(item=>{
        let html = `
        <tr class='rowdata' key='${item.id}'>
            <td>${item.id}</td>
            <td>${item.headline}</td>
            <td>${item.abstract}</td>
            <td>${item.content}</td>
            <td>${item.classify}</td>
            <td>${item.author}</td>
            <td>${item.postedtime}</td>
            <td>
                <input type="button" value="编辑" onclick="update(${item.id})">
                <input type="button" value="删除" onclick="del(${item.id})">
            </td>
        </tr>
        `
        $('table').append(html)
    })
}
}