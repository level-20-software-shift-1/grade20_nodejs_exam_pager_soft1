'use strict'
const {DataTypes} = require('sequelize')

let model ={
    headline:{
        type:DataTypes.STRING,
        allowNull:false
    },
    abstract:{
        type:DataTypes.DECIMAL,
        allowNull:false
    },
    content:{
        type:DataTypes.INTEGER,
        allowNull:false
    },
    classify:{
        type:DataTypes.INTEGER,
        allowNull:false
    },
    author:{
        type:DataTypes.INTEGER,
        allowNull:false
    },
}

module.exports = xujp1