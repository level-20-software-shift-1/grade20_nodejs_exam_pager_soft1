﻿'use strict'

const koa = require('koa');
const cors = require('koa-cors');
const bodyparser = require('koa-bodyparser');
const router = require('./router');
const { Blog, sync } = require('./model');

let app = new koa();

let app = new koa();
const sequelize = new Sequelize('xujp1','postgres','123456',{
    host:'xujp.top',
    dialect:'postgres'
})

sequelize.authenticate().then(()=>{
    try {
        console.log(1);
    } catch (error) {
        console.log(2);
    }
})

const products = sequelize.define('product',product)
sequelize.sync({force:true}).then(()=>{
    products.bulkCreate([
        {
            headline:'论EF Core 的自我修养',
            abstract:'没看过，不知道',
            content:'没看过，不知道',
            classify:'.net',
            author:'我不造啊',
            postedtime:'公元前20000世纪//可能是恐龙写的'
        },
        {
            headline:'DDD之我看不见',
            abstract:'盲人看美女',
            content:'全靠想象',
            classify:'盲人看到书',
            author:'盲僧-李青',
            postedtime:'1010-10-10：25：61：59'
        },
     {
        headline:'2',
        abstract:'没看过，不知道',
        content:'没看过，不知道',
        classify:'.net',
        author:'我不造啊',
        postedtime:'公元前20000世纪//恐龙写的'
        },
        {
            headline:'3',
            abstract:'没看过，不知道',
            content:'我不造啊',
            classify:'.net',
            author:'可能是恐龙写的',
            postedtime:'公元前20000世纪//可能是恐龙写的'
        },
        {
            headline:'4',
            abstract:'没看过，不知道',
            content:'没看过，不知道',
            classify:'.net',
            author:'我不造啊',
            postedtime:'公元前20000世纪//可能是恐龙写的'
        },
        
    ])
})

app.use(cors())
app.use(bodyparser())

router.get('/blog',async(ctx,next)=>{
    let keyword = ctx.request.query.keyword
    let list
    if(keyword){
        list = await blogs.findAll({
            where:{
                [Op.or]:[
                    {id:isNaN(keyword)?0:parseInt(keyword)},
                    {headline:keyword},
                    {abstract:keyword},
                    {content:keyword},
                    {classify :keyword},
                    {author:keyword}
                ]
            },
            order:['id']
        })
        ctx.body={
            code:10001,
            data:list,
            msg:'请求成功'
        }
    }else{
        list = await blogs.findAll({
            order:['id']
        })
        ctx.body={
            code:10001,
            data:list,
            msg:'请求成功'
        }
    }
})
router.get('/blog/:id',async(ctx,next)=>{
    let id = ctx.request.params.id
    let list = await blogs.findByPk(id)
    ctx.body={
        code:10002,
        data:list,
        msg:'查找成功'
    }
})
router.post('/blog',async(ctx,next)=>{
    let obj = ctx.request.body
    console.log(obj);
    let list = await blogs.create(obj)
    ctx.body={
        code:10003,
        data:list,
        msg:'添加成功'
    }
})
router.put('/blog/:id',async(ctx,next)=>{
    let id = ctx.request.params.id
    let obj = ctx.request.body

    let list = await blogs.update(obj,{
        where:{
            id:id
        }
    })
    ctx.body={
        code:10004,
        data:list,
        msg:'修改成功'
    }
    
})
router.delete('/blog/:id',async(ctx,next)=>{
    let id = ctx.request.params.id
    console.log(id);
    await blogs.destroy({
        where:{
            id:id
        }
    })
    ctx.body={
        code:10005,
        data:{id},
        msg:'删除成功'
    }
})

app.use(router.routes())

app.listen(3000,()=>{
    console.log(`http://localhost:3000`);
})