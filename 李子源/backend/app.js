'use strict';

const Koa=require('koa');
const bodyParser=require('koa-bodyparser');
const cors=require('koa-cors');
const {Products,sync } = require('./model');
const routerMiddleware = require('./router/index');


sync().then(()=>{
    Products.bulkCreate([
        {
            id: 1,
            title: "论EF Core的自我修养",
            digest: "论EF Core的自我修养",
            matter: "论EF Core的自我修养",
            classify: ".Net",
            author: "InCerry",
        },
        {
            id: 2,
            title: "DDD之我见",
            digest: "DDD之我见",
            matter: "DDD之我见",
            classify: "编程技术",
            author: "某大神",
        },
        {
            id: 3,
            title: "nginx负载平衡的几种策略",
            digest: "nginx负载平衡的几种策略",
            matter: "nginx负载平衡的几种策略",
            classify: "服务器",
            author: "老胡来也",
        },
        {
            id: 4,
            title: "Linux用户创建的学习研究",
            digest: "Linux用户创建的学习研究",
            matter: "Linux用户创建的学习研究",
            classify: "Linux",
            author: "某大神",
        },
        {
            id: 5,
            title: "大数据仪表盘探讨",
            digest: "大数据仪表盘探讨",
            matter: "大数据仪表盘探讨",
            classify: "大数据",
            author: "居家博士",
        },
    ])
})

let app = new Koa();

app.use(cors());
app.use(bodyParser());
app.use(routerMiddleware());

let port=4000;
app.listen(port);
console.log(`服务器在http://localhost:${port}上运行`);