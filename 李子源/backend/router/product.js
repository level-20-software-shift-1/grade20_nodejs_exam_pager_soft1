'use strict'

let { Products, Op } = require('../model');
let products = [
    {
        id: 1,
        title: "论EF Core的自我修养",
        digest: "论EF Core的自我修养",
        matter: "论EF Core的自我修养",
        classify: ".Net",
        author: "InCerry",
    },
    {
        id: 2,
        title: "DDD之我见",
        digest: "DDD之我见",
        matter: "DDD之我见",
        classify: "编程技术",
        author: "某大神",
    },
    {
        id: 3,
        title: "nginx负载平衡的几种策略",
        digest: "nginx负载平衡的几种策略",
        matter: "nginx负载平衡的几种策略",
        classify: "服务器",
        author: "老胡来也",
    },
    {
        id: 4,
        title: "Linux用户创建的学习研究",
        digest: "Linux用户创建的学习研究",
        matter: "Linux用户创建的学习研究",
        classify: "Linux",
        author: "某大神",
    },
    {
        id: 5,
        title: "大数据仪表盘探讨",
        digest: "大数据仪表盘探讨",
        matter: "大数据仪表盘探讨",
        classify: "大数据",
        author: "居家博士",
    },
]

let fn_list = async (ctx, next) => {
    let keyword = ctx.request.query.keyword || '';
    console.log(keyword.length);

    if (keyword) {
        let list;
        list = await Products.findAll({
            where: {
                [Op.or]: [
                    { id: isNaN(keyword) ? 0 : parseInt(keyword) },
                    { productName: keyword },
                    { price: isNaN(keyword) ? 0 : keyword },
                    { stockNum: isNaN(keyword) ? 0 : parseInt(keyword) },
                    { supplier: keyword },
                ]
            },
            order: ['id']
        })
        ctx.body = {
            code: 1000,
            data: list,
            msg: '已请求'
        }
    } else {
        let list = await Products.findAll({
            order: ['id']
        });
        ctx.body = {
            code: 1000,
            data: products,
            msg: '已请求'
        }
    }
}
//查找
let fn_getbyid = async (ctx, next) => {
    let id = ctx.request.params.id;
    let product = await Products.findByPk(id);
    if (product) {
        ctx.body = {
            code: 1000,
            data: filterList[0],
            msg: '获取成功'
        }
    } else {
        ctx.body = {
            code: 900,
            data: '',
            msg: '不存在'
        }
    }
}

let fn_post = async (ctx, next) => {
    let obj = ctx.request.body;
    console.log(obj);

    await Products.create(obj);
    ctx.body = obj;
}
//修改
let fn_put = async (ctx, next) => {
    let id = ctx.request.params.id;
    let obj = ctx.request.body;
    let product = await Products.findByPk(id);
    if (product) {
        Products.update(obj, {
            where: {
                id: id
            }
        })
        ctx.body = {
            code: 1000,
            data: product,
            msg: '修改成功'
        }
    } else {
        ctx.body = {
            code: 900,
            data: '',
            msg: '不存在'
        }
    }
}

//删除
let fn_delete = async (ctx, next) => {
    let id = ctx.request.params.id;
    console.log(id);
    let product = await Products.findByPk(id)
    if (product) {
        Products.destroy({
            where: {
                id: id
            }
        })
        ctx.body = {
            code: 1000,
            data: { id },
            msg: '删除成功'
        }
    } else {
        ctx.body = {
            code: 400,
            data: null,
            msg: '不存在'
        }
    }
}

module.exports = {
    'get /product': fn_list,
    'get /product/:id': fn_getbyid,
    'post /product': fn_post,
    'put /product/:id': fn_put,
    'delete /product/:id': fn_delete,
}