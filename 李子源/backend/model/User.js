'use strict';
const { DataTypes } = require('sequelize');

let model = {
    username: {
        type: DataTypes.STRING,
        allowNull: false
    },
    password: {
        type: DataTypes.STRING
    }
}

module.exports=model;