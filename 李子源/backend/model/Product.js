'use strict';
const { DataTypes } = require('sequelize');

let model = {
    productName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    price:{
        type:DataTypes.DECIMAL,
        allowNull:false,
        default:0
    },
    stockNum:{
        type:DataTypes.INTEGER,
        allowNull:false,
        default:100
    },
    supplier:{
        type:DataTypes.STRING,
        allowNull:false
    }
    
}

module.exports=model;