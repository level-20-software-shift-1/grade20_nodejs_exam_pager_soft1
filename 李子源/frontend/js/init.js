'use strict';

//加载事件
$(function () {
    getListWithKeyword();
})

function add() {
    console.log('点击一下按钮');
    location.href = './addOrEdit.html';
}

function getListWithKeyword(keyword) {
    keyword = keyword || ''
    console.log(keyword);
    getProductList(keyword).then(res => {
        console.log(res);
        render(res.data);
    })
}

function render(data) {
    let tb = $('#tb');
    let row = $('.row');
    row.remove();
    data.forEach(item => {
        let html = `
            <tr class="row" key="${item.id}">
                <td>${item.id}</td>
                <td>${item.title}</td>
                <td>${item.digest}</td>
                <td>${item.matter}</td>
                <td>${item.classify}</td>
                <td>${item.author}</td>
                <td>
                    <input type="button" value="修改" onclick="updata(${item.id})">
                    <input type="button" value="删除" onclick="del(${item.id})">
                </td>
            </tr>
        `
        tb.append(html);
    });
}