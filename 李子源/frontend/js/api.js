'use strict';

//获取商品
function getProductList(keyword) {
    console.log(1);
    return new Promise(function (resolve, reject) {
        $.get(`http://localhost:4000/product?keyword=` + keyword, (data) => {
            resolve(data);
            
        })
    })
}

//根据id获取商品
function getProductById(id) {
    return new Promise(function (abc, bbc) {
        $.get(`http://localhost:4000/product/${id}`, data => {
            abc(data);
        })
    })
}

//新增
function addProduct(obj) {
    return new Promise(function (resolve, reject) {
        $.post(`http://localhost:4000/product`, obj, (data) => {
            resolve(data);
        })
    })
}

//修改
function updateProduct(id, obj) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `http://localhost:4000/product/${id}`,
            type: "PUT",
            data: obj,
            success: function (data) {
                resolve(data);
            }
        });
    })
}

//删除
function delProductById(id) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `http://localhost:4000/product/${id}`,
            type: "delete",
            success: function (data) {
                resolve(data);
            }
        });
    })
}