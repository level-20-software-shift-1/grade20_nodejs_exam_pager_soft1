const { Sequelize, Op } = require('sequelize')

const fs = require('fs')
const path = require('path')



const sequelize = new Sequelize('demo', 'postgres', '123456', {
    host: '1durance.ltd',
    dialect: 'postgres'
})


let obj = {}

obj.Op = Op

let files = fs.readdirSync(path.join(__dirname))

tableFile = files.filter((item) => {

    return item !== 'sequelize.js' && item.endsWith('.js')

})

tableFile.forEach(item => {
    let fileName = item.replace('.js', '')

    let name = fileName.toLowerCase()

    let table = require(path.join(__dirname, item))


    obj[fileName] = sequelize.define(name, table)

});


//sequelize.sync({force:true})



obj.sequelizeSync = () => {

    sequelize.sync({ force: true }).then(res => {

        obj.Student.bulkCreate(
            [{
                   title: '论EF Code的自我修养',
                   abstract: '80',
                    gander: '女'
                },
                {
                    name: 'qian',
                    age: '18',
                    gander: '女'
                },
                {
                    name: 'sun',
                    age: '13',
                    gander: '女'
                },
            ]
        )
        obj.User.bulkCreate(
            [{
                    account: 'li',
                    password: 12345
                },
                {
                    account: 'zhou',
                    password: 12345
                }
            ]
        )

    })
}


module.exports = obj