'use strict'

let koa = require('koa');
let bodyparser = require('koa-bodyparser');
let cors = require('koa-cors');
let router = require('./router');
let { Bolg, sync } = require('./modul');

// 进行同步远端数据库
sync().then(() => {
    Bolg.bulkCreate([
        {
            title: '论EF Core的自我修养',
            digest: '论EF Core的自我修养',
            content: '论EF Core的自我修养',
            variety: '.Net',
            author: 'InCerry'
        },
        {
            title: 'DDD之我见',
            digest: 'DDD之我见',
            content: 'DDD之我见',
            variety: '编程技术',
            author: '某大神'
        },

    ])
})

let app = new koa();

app.use(cors());
app.use(bodyparser());
app.use(router());

let port = 3000;
app.listen(port);
console.log('success');