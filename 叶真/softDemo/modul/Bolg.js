'use strict'

let { DataTypes } = require('sequelize')

let modul = {
    // 标题
    title: {
        type: DataTypes.STRING
    },

    //摘要
    digest: {
        type: DataTypes.STRING
    },

    //内容
    content: {
        type: DataTypes.STRING
    },

    //分类
    variety: {
        type: DataTypes.STRING
    },

    //作者
    author: {
        type: DataTypes.STRING
    }
}

module.exports = modul;