'use strict'

let fs = require('fs');
let { Op, Sequelize } = require('sequelize');
const { time } = require('console');

let sequelize = new Sequelize('bolg', 'postgres', '100110', {
    dialect: 'postgres',
    host: 'catdogdiary.top'
})

let tmpFiles = fs.readdirSync(__dirname);
let files = tmpFiles.filter(item => {
    return item.endsWith('.js') && item !== 'index.js';
})

let resultModul = {};

files.forEach(item => {
    let modulName = item.replace('.js', '');
    let modul = require(__dirname + '/' + item);
    let tableName = modulName.toLowerCase();
    resultModul[modulName] = sequelize.define(tableName, modul);
});

resultModul.Op = Op;
resultModul.sequelize = sequelize;
resultModul.sync = async () => {
    return await sequelize.sync({ force: true });
}

module.exports = resultModul;