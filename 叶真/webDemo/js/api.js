'use strict'

/* 
    获得keyword ，并判断是否存在，
        如果不存在那么视为重新加载页面，获得并返回全部的数据到渲染函数
        如果存在，那么查找到用户想要的数据，并返回给渲染函数
*/
function getDataByKeyword(keyword) {
    return new Promise(function (resolve, reject) {
        $.get(`${url}/bolg?keyword=${keyword}`, (res) => {
            let tmp = res.data;

            resolve(tmp);
        })
    })
}
/* 
    通过id查找到用户需要的数据
*/
function getDataById(id) {
    return new Promise(function (resolve, reject) {
        $.get(`${url}/bolg/${id}`, (res) => {
            let tmp = res.data;
            resolve(tmp);
        })
    })
}

// 添加一条用户输入的数据
function addData(obj) {
    return new Promise(function (resolve, reject) {
        $.post(`${url}/bolg`, obj, (res) => {
            resolve();
        })
    })
}

// 更新一条用户输入的数据
function updateData(id, obj) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `${url}/bolg/${id}`,
            data: obj,
            type: 'PUT',
            success: function () {
                resolve();
            }
        })
    })
}

// 删除一条数据
function deleteData(id) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `${url}/bolg/${id}`,
            type: 'DELETE',
            success: function (res) {
                let tmp = res.data;
                resolve(tmp);
            }
        })
    })
}