'use strict'

$(function () {
    getProductByKeyword();
})

/* 
    获得keyword ，并判断是否存在，
        如果不存在那么视为重新加载页面，获得并返回全部的数据到渲染函数
        如果存在，那么查找到用户想要的数据，并返回给渲染函数
*/
function getProductByKeyword(keyword) {
    let tmp = keyword || '';

    getDataByKeyword(tmp).then((res) => {
        replay(res);
    })
}

// 重新渲染主页面显示的数据库数据
function replay(data) {
    let tb = $('#tb');
    let tbData = $('.tbData');
    tbData.remove();
    let h;

    data.forEach(item => {
        h = `
            <tr class="tbData">
            <td>${item.id}</td>
            <td>${item.title}</td>
            <td>${item.digest}</td>
            <td>${item.content}</td>
            <td>${item.variety}</td>
            <td>${item.author}</td>
            <td>${item.createdAt}</td>
            <td>
                <input type="button" value="编辑" onclick="edit(${item.id})">
                <input type="button" value="删除" onclick="del(${item.id})">
            </td>
        </tr>
            `
            tb.append(h);
    });
}

