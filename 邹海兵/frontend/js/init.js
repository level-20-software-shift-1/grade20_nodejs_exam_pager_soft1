'use strict'

$(function(){
    getListWithKwyword()
})

function getListWithKwyword(keyword){
    keyword = keyword || ''
    getBlogList(keyword).then(res=>{
        render(res.data)
    })
}

function render(res){
    console.log(res);
    $('.rowdata').remove()
    res.forEach(item=>{
        let html = `
        <tr class='rowdata' key='${item.id}'>
            <td>${item.id}</td>
            <td>${item.title}</td>
            <td>${item.zhaoyao}</td>
            <td>${item.neirong}</td>
            <td>${item.fenlei}</td>
            <td>${item.zuozhe}</td>
            <td>${item.updatedAt}</td>
            <td>
                <input type="button" value="编辑" onclick="update(${item.id})">
                <input type="button" value="删除" onclick="del(${item.id})">
            </td>
        </tr>
        `
        $('table').append(html)
    })
}