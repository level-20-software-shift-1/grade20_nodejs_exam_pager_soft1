'use strict'

// 列表
function getBlogList(keyword){
    return new Promise(function(resolve,reject){
        $.get(`${baseUrl}/blog?keyword=${keyword}`,res=>{
            resolve(res)
        })
    })
}

// 根据id查找
function getListById(id){
    return new Promise(function(resolve,reject){
        $.get(`${baseUrl}/blog/${id}`,res=>{
            resolve(res)
        })
    })
}

//添加
function addBlog(obj){
    return new Promise(function(resolve,reject){
        $.post(`${baseUrl}/blog`,obj,res=>{
            resolve(res)
        })
    })
}

// 编辑
function updateBlog(id,obj){
    return new Promise(function(resolve,reject){
       $.ajax({
           url:`${baseUrl}/blog/${id}`,
           data:obj,
           type:'put',
           success:function(res){
               resolve(res)
           }
       })
    })
}

// 删除
function delBlog(id){
    return new Promise(function(resolve,reject){
       $.ajax({
           url:`${baseUrl}/blog/${id}`,
           data:'',
           type:'delete',
           success:function(res){
               resolve(res)
           }
       })
    })
}