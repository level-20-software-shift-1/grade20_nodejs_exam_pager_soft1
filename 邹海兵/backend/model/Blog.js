'use strict'
const {DataTypes} = require('sequelize')
let model = {
    title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    zhaoyao:{
        type:DataTypes.STRING,
        allowNull:false
    },
    neirong:{
        type:DataTypes.STRING,
        allowNull:false
    },
    fenlei:{
        type:DataTypes.STRING,
        allowNull:false
    },
    zuozhe:{
        type:DataTypes.STRING,
        allowNull:false
    }
}

module.exports = model