'use strict';
function getBolgList(keyword) {
    return new Promise(function (resolve, reject) {
        $.get(`${baseUrl}/bolg?keyword=`+keyword, (data) => {
            resolve(data);
        })
    })
}
function getBolgById(id) {
    return new Promise(function (abc, bbc) {
        $.get(`${baseUrl}/bolg/${id}`, data => {
            abc(data);
        })
    })
}
function addBolg(obj) {
    return new Promise(function (resolve, reject) {
        $.post(`${baseUrl}/bolg`, obj, (data) => {
            resolve(data);
        })
    })
}
function updateBolg(id, obj) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `${baseUrl}/bolg/${id}`,
            type: "PUT",
            data: obj,
            success: function (data) {
                resolve(data);
            }
        });
 
    })
}
function delBolgById(id) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `${baseUrl}/bolg/${id}`,
            type: "delete",
            success: function (data) {
                resolve(data);
            }
        });
    })
}