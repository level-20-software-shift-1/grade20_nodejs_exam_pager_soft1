'use strict'
const Koa=require('koa')
const bodyparser=require('koa-bodyparser')
const cors=require('koa-cors')
const router=require('./router/router.js')


let app=new Koa();
app.use(cors());
app.use(bodyparser());
app.use(router.routes());

let port =8888;
app.listen(port);
console.log(`服务器运行在：http://localhost:8888`);