'use strict'

function add(){
    location.href='./add.html'
}

$(function (){
    getListWithKeyword()
})

function getListWithKeyword(keyword){
    keyword =keyword || '';
    getBlogList(keyword).then(res=>{
        render(res.data)
    })
}
//重新渲染列表
function render(data){
    let tb=$('#tbData')
    let rowData=$('.rowData')
    rowData.remove();
    data.forEach(item=>{
        let h=`
        <tr class="rowData" key="${item.id}">
        <td>${item.id}</td>
        <td>${item.title}</td>
        <td>${item.abstract}</td>
        <td>${item.content}</td>
        <td>${item.classify}</td>
        <td>${item.author}</td>
        <td><input type="button" value="修改" onclick="update(${item.id})"></td>
        <td><input type="button" value="删除" onclick="del(${item.id})"></td>
    </tr>
        `
        tb.append(h)
    })
}