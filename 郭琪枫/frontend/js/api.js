'use strict'
//获取博客列表
function getBlogList(keyword){
    return new Promise(function (resolve,reject){
        $.get(`${baseUrl}/blog?keyword=`+keyword,data=>{
            resolve(data)
        })
    })
}
//通过id获取
function getBlogById(id){
    return new Promise(function (resolve,reject){
        $.get(`${baseUrl}/blog/${id}`,data=>{
            resolve(data)
        })
    })
}
//添加博客
function addBlog(obj){
    return new Promise(function (resolve,reject){
        $.post(`${baseUrl}/blog`,obj,data=>{
            resolve(data)
        })
    })
}
//修改博客
function updateBlog(id,obj){
    return new Promise(function (resolve,reject){
        $.ajax({
            url:`${baseUrl}/blog/${id}`,
            type:'PUT',
            data:obj,
            success:function (data){
                resolve(data)
            }
        })
    })
}
//删除博客
function delBlog(id){
    return new Promise(function (resolve,reject){
        $.ajax({
            url:`${baseUrl}/blog/${id}`,
            type:'DELETE',
            success:function (data){
                resolve(data)
            }
        })
    })
}