'use strict'
//引入模块
const koa=require('koa')
const cors=require('koa-cors')
const bodyParser=require('koa-bodyparser')
const {Blogs,sync}=require('./model')
const routerMiddleware=require('./router/index')
//批量添加数据
sync().then(()=>{
    Blogs.bulkCreate([
        {
            title:'重生之我在乌克兰当雇佣兵',
            abstract:'俄罗斯乌克兰战况',
            content:'一觉醒来我竟然到了乌克兰',
            classify:'穿越',
            author:'郭半仙'
        }
    ])
})

let app=new koa();
app.use(cors()).use(bodyParser()).use(routerMiddleware())
let port=3000;
//监听端口
app.listen(port)