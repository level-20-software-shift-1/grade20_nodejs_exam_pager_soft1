'use strict'

const {Sequelize,Op}=require('sequelize')
const {db_database,db_host,db_password,db_dialect,db_username}=require('../config/db')
const fs=require('fs')

//尝试连接数据库
const sequelize=new Sequelize(db_database,db_username,db_password,{
    host:db_host,
    dialect:db_dialect
})


let files=fs.readdirSync(__dirname);

let resFiles=files.filter(item=>{
    return item.endsWith('.js') && item !== 'index.js'
})

let resultModel={};
//遍历所有路由文件
resFiles.forEach(item=>{
    let modelName=item.replace('.js','')
    let model=require(__dirname+'/'+item)
    let tableName=modelName.toLowerCase();
    resultModel[modelName]=sequelize.define(tableName,model)
})
//挂载sequelize实体对象
resultModel.sequelize=sequelize;
resultModel.Op=Op;

resultModel.sync=async()=>{
    return await sequelize.sync({force:true})
}

module.exports=resultModel;