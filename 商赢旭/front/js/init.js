'use strict';
$(function () {
    getListWithKeyword();
})
function add() {
    location.href = './addOrEdit.html';
}
function getListWithKeyword(keyword) {
    keyword = keyword || ''
    if (keyword!='') {
    }
    
    getProductList(keyword).then(res => {
        render(res.data);
    })
}
function render(data) {
    let tb = $('#tbData');
    let rowData = $('.rowData');
    rowData.remove();
    console.log(data);
    data.forEach(item => {
        let h = `
            <tr class="rowData" key="${item.id}">
                <td>${item.headline}</td>
                <td>${item.abstract}</td>
                <td>${item.content}</td>
                <td>${item.classify}</td>
                <td>${item.author}</td>
                <td>
                <input type="button" value="编辑" onclick="update(${item.id})">
                <input type="button" value="删除" onclick="del(${item.id})">
                </td>
            </tr>
            `
        tb.append(h);
    })
}