'use strict'
const router=require('koa-router')();
const fs=require('fs');
let arrfile=fs.readdirSync(__dirname);
console.log(arrfile);
let newarrfile=arrfile.filter(item=>{
    return item.endsWith('.js')&&item!=='index.js'
});
newarrfile.forEach((item)=>{
    let a =require(__dirname+"/"+item)
    for(let key in a){
        let Method=key.split(' ')[0]
        let Path=key.split(' ')[1]
        console.log(Method,Path);
        let Function=a[key];
        if (Method === 'get'){
            router.get(Path,Function)
        }else if (Method==='post') {
            router.post(Path,Function)
        }else if (Method==='put') {
            router.put(Path,Function)
        }else if (Method === 'delete') {
            router.delete(Path,Function)
        }else{

        }
    }
})
module.exports=router.routes();