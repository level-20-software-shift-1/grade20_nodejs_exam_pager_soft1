// headline 标题 abstract 摘要 content内容 classify 分类 author 作者 release time 发布时间
const { DataTypes }=require('sequelize');

let model={
    headline:{
        type: DataTypes.STRING,
        allowNull:false
    },
    abstract:{
        type:DataTypes.STRING,
        allowNull:false
    },
    conten:{
        type:DataTypes.STRING,
        allowNull:false
    },
    classify:{
        type:DataTypes.STRING,
        allowNull:false
    }, 
    author:{
        type:DataTypes.STRING,
        allowNull:false
    },
}
module.exports=model;