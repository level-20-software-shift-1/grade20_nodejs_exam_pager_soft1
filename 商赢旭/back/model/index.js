const { Sequelize,Op,DataTypes} = require('sequelize');
//数据库连接
const sequelize = new Sequelize('liuyi', 'postgres', '123456', {
    host: 'huozhu.ltd',
    dialect: 'postgres'
  });
//遍历文件夹
const fs =require('fs');
let file = fs.readdirSync(__dirname);
let newfile=file.filter(
    item =>{
        return item.endsWith('.js')&&item!=('index.js');
    }
)
//同步数据库
let resultmodel={};
newfile.forEach(
    item =>{
        let modelName= item.replace('.js','');
        let model= require(__dirname+'/'+item);
        let tableName=modelName.toLowerCase();
        resultmodel[modelName]=sequelize.define(tableName,model)}
)

resultmodel.sequelize= sequelize;
resultmodel.Op=Op;
resultmodel.sync =async()=>{
    return await sequelize.sync({force:true});
}
module.exports=resultmodel;