'use strict'
//引入各种需要的包
const Koa = require("koa");
const cors = require("koa-cors");
const body= require("koa-body");
const { User,sync }=require('./model'); 
const router=require('./router/index')
//初始化koa对象
let app =new Koa();
//设置跨域
app.use(cors()).use(body());
//启动数据库
app.use(router)
// sync().then(()=>{
//     Blog.bulkCreate(
//         [
//             {
//                 // 标题 
//                 headline:'论EF code的自我修养',
//                 //摘要 
//                  abstract:'论EF code的自我修养',
//                 //内容 
//                   content:'论EF code的自我修养',
//                 //分类
//                   classify:'.Net',
//                 //作者 
//                    author :'inCerry',
//                 //发布时间
//                    releasetime :'2022-04-06 08:47',
                   
//             },
//             {
//                 // 标题 
//                 headline:'DDD之我见',
//                 //摘要 
//                  abstract:'DDD之我见',
//                 //内容 
//                   content:'DDD之我见',
//                 //分类
//                   classify:'编程技术',
//                 //作者 
//                    author :'某大神',
//                 //发布时间
//                    releasetime :'2022-04-03 23:47',
                   
//             }, 
//             {
//                 // 标题 
//                 headline:'nginx负载品面的几种策略',
//                 //摘要 
//                  abstract:'nginx负载品面的几种策略',
//                 //内容 
//                   content:'nginx负载品面的几种策略',
//                 //分类
//                   classify:'服务器',
//                 //作者 
//                    author :'老胡来也',
//                 //发布时间
//                    releasetime :'2022-04-06 08:47',
                   
//             } ,
//             {
//                 // 标题 
//                 headline:'Linux用户创建学习研究',
//                 //摘要 
//                  abstract:'Linux用户创建学习研究',
//                 //内容 
//                   content:'Linux用户创建学习研究',
//                 //分类
//                   classify:'Linux',
//                 //作者 
//                    author :'某大神',
//                 //发布时间
//                    releasetime :'2022-04-06 08:47',
                   
//             },
//              {
//                 // 标题 
//                 headline:'大数据仪表盘探讨',
//                 //摘要 
//                  abstract:'大数据仪表盘探讨',
//                 //内容 
//                   content:'大数据仪表盘探讨',
//                 //分类
//                   classify:'大数据',
//                 //作者 
//                    author :'居家博士',
//                 //发布时间
//                    releasetime :'2022-04-18 16:18',
                   
//             },
//         ]
//     )
// })
//监听端口
let port=3000
app.listen(port);
console.log(`服务器运行在http://localhost:${port}`);