'use strict'
$(function () {
    let rid = getUrlid('id') || 0;

    fn_getById(rid).then(
        res => {
            $('[name=title]').val(res.data.title);
            $('[name=talk]').val(res.data.talk);
            $('[name=context]').val(res.data.context);
            $('[name=sort]').val(res.data.sort);
            $('[name=author]').val(res.data.author);
            $('[name=Ftime]').val(res.data.Ftime);
        }
    )
})

function getUrlid(name) {
    let rs = new RegExp('(^|&)' + name + '=([^&]*)(&|$)');
    let r = location.search.substr(1).match(rs);
    if (r !== null) {
        return unescape(r[2]);
    } else {
        return null;
    }
}

function save() {
    let id = getUrlid('id');
    let obj = {
        title: $('[name=title]').val(),
        talk: $('[name=talk]').val(),
        context: $('[name=context]').val(),
        sort: $('[name=sort]').val(),
        author: $('[name=author]').val(),
        Ftime: $('[name=Ftime]').val()
    }
    if (id) {
        fn_putUp(id, obj).then(res => {
            location.href = './index.html'
        })
    } else {
        let objobj = {
            title: obj.title,
            talk: obj.talk,
            context: obj.context,
            sort: obj.sort,
            author: obj.author,
            Ftime: obj.Ftime
        }
        fn_postAdd(objobj).then(res => {
            location.href = './index.html'
        })
    }
}

function clice() {
    location.href = './index.html'
}