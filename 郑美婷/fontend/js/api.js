'use strict'
function fn_getList(keyword) {
    return new Promise((resolve, reject) => {
        $.get(`${baseUrl}?keyword=${keyword}`, res => {
            resolve(res);
        })
    })
}

function fn_getById(id) {
    return new Promise((resolve, reject) => {
        $.get(`${baseUrl}/${id}`, res => {
            resolve(res);
        })
    })
}

function fn_postAdd(obj) {
    return new Promise((resolve, reject) => {
        $.post(`${baseUrl}`, obj, res => {
            resolve(res);
        })
    })
}

function fn_putUp(id, obj) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `${baseUrl}/${id}`,
            data: obj,
            type: 'put',
            success: res => {
                resolve(res)
            }
        })
    })
}

function fn_deletDel(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: `${baseUrl}/${id}`,
            data: '',
            type: 'delete',
            success: res => {
                resolve(res)
            }
        })
    })
}