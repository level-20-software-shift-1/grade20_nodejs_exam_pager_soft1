'use strict'
$(function () {
    getKeyWordList();
})

function getKeyWordList(keyword) {
    let keywords = keyword || ""
    fn_getList(keywords).then(res => {
        console.log(res.data);
        tabAppend(res.data);
    });
}

function add() {
    location.href = './addOrUp.html'
}

function up(id) {
    location.href = './addOrUp.html?id=' + id
}

function del(id) {
    if (confirm('确定删除吗？')) {
        fn_deletDel(id).then(res => {
            if (res.code === 1000) {
                let id = res.data.id;
                $(`[key=${id}]`).remove();
                location.reload();
            } else {
                console.log(res.msg);
            }
        })
    } else {
        console.log('取消了删除！');
    }
}

function sel() {
    let keyword = $('[name=keyword]').val();
    getKeyWordList(keyword);
}

function tabAppend(arr) {
    let rowTr = $('.rowTr');
    let tab = $('.tab');
    rowTr.remove();
    console.log(arr);
    arr.forEach(i => {
        let html =
            `
            <tr class="rowTr" key="${i.id}">
            <td>${i.id}</td>
            <td>${i.title}</td>
            <td>${i.talk}</td>
            <td>${i.context}</td>
            <td>${i.sort}</td>
            <td>${i.author}</td>
            <td>${i.Ftime}</td>
            <td>
                <input type="button" value="修改" onclick="up(${i.id})">
                <input type="button" value="删除" onclick="del(${i.id})">
            </td>
            </tr>
            `
        tab.append(html)
    });
}