'use strict'
let { db_database, db_username, db_password, db_host, db_dialect } = require('../config/db');
let { Sequelize, Op } = require('sequelize');
let fs = require('fs');
let path = require('path');

let sequelize = new Sequelize(db_database, db_username, db_password,
    {
        host: db_host,
        dialect: db_dialect
    }
)

let fPath = __dirname

let fArr = fs.readdirSync(fPath);

let Ffilter = fArr.filter(i => {
    return i.endsWith('.js') && i !== 'index.js'
});
let obj = {}

Ffilter.forEach(i => {
    let Fpath = require(path.join(__dirname, i));

    let Freplace = i.replace('.js', '');
    let Flower = Freplace.toLowerCase();

    obj[Freplace] = sequelize.define(Flower, Fpath);
});

obj.Op = Op;
obj.sequelize = sequelize;

obj.sync = async () => {
    return await sequelize.sync({ force: true })
}

module.exports = obj