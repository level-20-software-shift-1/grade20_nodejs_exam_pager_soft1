'use strict'
let { DataTypes } = require('sequelize');

let model = {
    title: DataTypes.STRING,
    talk: DataTypes.STRING,
    context: DataTypes.STRING,
    sort: DataTypes.STRING,
    author: DataTypes.STRING,
    Ftime: DataTypes.STRING
}

module.exports = model