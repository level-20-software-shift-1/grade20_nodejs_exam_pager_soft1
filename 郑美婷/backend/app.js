'use strict'
const Koa = require('koa');
const bodyparser = require('koa-bodyparser');
const cors = require('koa-cors');

const routerMidder = require('./router/index')();
const { Contexts, sync } = require('./model');

sync().then(() => {
    Contexts.bulkCreate([
        {
            title: '论EFCore的自我修养',
            talk: '论EFCore的自我修养',
            context: '论EFCore的自我修养',
            sort: '.Net',
            author: 'InCerry',
            Ftime: '2022-04-06'
        },
        {
            title: '论EFCore的自我修养',
            talk: '论EFCore的自我修养',
            context: '论EFCore的自我修养',
            sort: '.Net',
            author: 'InCerry',
            Ftime: '2022-04-06'
        },
        {
            title: '论EFCore的自我修养',
            talk: '论EFCore的自我修养',
            context: '论EFCore的自我修养',
            sort: '.Net',
            author: 'InCerry',
            Ftime: '2022-04-06'
        },
        {
            title: '论EFCore的自我修养',
            talk: '论EFCore的自我修养',
            context: '论EFCore的自我修养',
            sort: '.Net',
            author: 'InCerry',
            Ftime: '2022-04-06'
        },
        {
            title: '论EFCore的自我修养',
            talk: '论EFCore的自我修养',
            context: '论EFCore的自我修养',
            sort: '.Net',
            author: 'InCerry',
            Ftime: '2022-04-06'
        }
    ])
})

let app = new Koa();
app.use(cors());
app.use(bodyparser());
app.use(routerMidder);

app.listen(3000, () => {
    console.log('服务器入口为：http://localhost:3000');
})