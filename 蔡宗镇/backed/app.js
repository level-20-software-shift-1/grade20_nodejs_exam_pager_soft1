const Koa = require("koa");
const body = require("koa-body");
const cors = require("koa-cors");

const app = new Koa();

let router = require('./rotuer');
app.use(body()).use(cors()).use(router)


app.listen(3000,(error)=>{
    if (error) {
        console.log(error);
        return ;
    }
    console.log("服务器启动成功");
});

