const {DataTypes} = require("sequelize");

let Blog = {
    title:{
        type:DataTypes.STRING,
        allowNull:false
    },
    abstract:{
        type:DataTypes.STRING,
        allowNull:false
    },
    content:{
        type:DataTypes.STRING,
        allowNull:false
    },
    classify:{
        type:DataTypes.STRING,
        allowNull:false
    },
    author:{
        type:DataTypes.STRING,
        allowNull:false
    },
    time:{
        type:DataTypes.DATE,
        allowNull:false,
        
    },
}

module.exports = Blog;