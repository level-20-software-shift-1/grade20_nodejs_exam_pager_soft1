

const {Op,Sequelize} = require("sequelize");

let obj = require("./Blog.js");

const sequelize = new Sequelize('soft1','postgres','510239621czz.',{
    host:'cailikes.fun',
    dialect:"postgres"
});


let Blog = sequelize.define('blog',obj);

sequelize.sync({force:true})
.then(()=>{
    Blog.bulkCreate([
        {
            title:'java',
            abstract:'java的世界',
            content:'学好java',
            classify:'后端',
            author:'大大',
            time:'2020-10-15'
        },
        {
            title:'C',
            abstract:'C的世界',
            content:'学好C',
            classify:'后端',
            author:'大大',
            time:'2020-10-15'
        },
    ]);
});

module.exports={Op,Blog}