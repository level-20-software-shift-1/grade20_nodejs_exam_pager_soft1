

const router = require('koa-router')();

let obj = require('./Blog.js');

for(let key in obj){
    let Modth = key.split(' ')[0];
    let URL = key.split(' ')[1];
    let FNC = obj[key];
    if (Modth==='get') {
        router.get(URL,FNC);
    }
    else if (Modth==='post') {
        router.post(URL,FNC);
    }
    else if (Modth==='delete') {
        router.delete(URL,FNC);
    }
    else if (Modth==='put') {
        router.put(URL,FNC);
    }
    else{
        console.log("请求的方式有误");
    }
}

module.exports = router.routes();