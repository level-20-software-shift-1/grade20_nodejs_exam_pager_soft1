//获取列表
function getList(arr) {
    let tb = $("[name=tb]");
    tb.html('');

    let html = `
        <tr>
            <th>ID</th>
            <th>标题</th>
            <th>摘要</th>
            <th>内容</th>
            <th>分类</th>
            <th>作者</th>
            <th>发表时间</th>
            <th>操作</th>
        </tr>
    `;
    tb.append(html);
    arr.forEach(item => {
        let html02 = `
        <tr>
        <td>${item.id}</td>
        <td>${item.title}</td>
        <td>${item.abstract}</td>
        <td>${item.content}</td>
        <td>${item.classify}</td>
        <td>${item.author}</td>
        <td>${item.time}</td>
        <td>
        <input type="button" value="编辑" onclick="fn_update(${item.id})">
        <input type="button" value="删除" onclick="fn_delete(${item.id})">
        </td>
    </tr>
        `
        tb.append(html02);
    });

}
//删除
function fn_delete(id) {
    if (confirm("确定要删除吗？")) {
        $.ajax({
            url: url + '/' + id,
            type: 'delete',
            success: (res) => {
                if (res.code === 1000) {
                    alert(res.message);
                    window.location.href = './index.html';
                }
                else {
                    alert('删除失败');
                    window.location.href = './index.html';
                }
            }
        });

    }
}

//编辑
function fn_update(id) {
    window.location.href = './enditro.html?id=' + id
}
//返回
function fn_return() {
    window.location.href = './index.html';
}

//添加
function fn_add() {
    window.location.href = './enditro.html?id='
}

// 修改、添加
function fn_alert() {
    let ID = $("#id").val()
    
    let title = $('#title').val();
    let abstract = $('#abstract').val();
    let content = $('#content').val();
    let classify = $('#classify').val();
    let author = $('#author').val();
    let obj = { title, abstract, content, classify, author }
    if (ID === '0') {
        $.ajax({
            url: url,
            type: 'post',
            data: obj,
            success: (res) => {
                console.log(res);
                    if (res.code === 1000) {
                        alert(res.message);
                        window.location.href = './index.html';
                    }
                    else {
                        alert('修改失败');
                        window.location.href = './index.html';
                    }
            }
        })
    }
    else {
        $.ajax({
            url: url + '/' + ID,
            type: 'put',
            data: obj,
            success: (res) => {
                    if (res.code === 1000) {
                        alert(res.message);
                        window.location.href = './index.html';
                    }
                    else {
                        alert('修改失败');
                        window.location.href = './index.html';
                    }
            }
        })
    }
}


//查找
function fn_select(){
    let Value = $('#Value').val();
    $.ajax({
        url: url + '/false',
        type: 'post',
        data: {Value},
        success: (res) => {
                if (res.code === 1000) {
                    alert(res.message);
                    getList(res.data)
                }
                else {
                    window.location.href = './index.html';
                }
        }
    })
}