'use strict'

const{Products,Op}=require('../model')

let fn_get= async(ctx,next)=>
{
    let keyword=ctx.request.query.keyword ||'';
    if(keyword)
    {
        let list =await Products.findAll(
            {
                where:
                {
                    [Op.or]:
                    {
                        id:isNaN(keyword)?0:parseInt(keyword),
                        biaoti:keyword,
                        zhayao:keyword,
                        neirong:keyword,
                        fenlei:keyword,
                        zuozhe:keyword,
                        shijian:parseInt(keyword),
                    }
                },
                order:['id']
            })
            ctx.body=list
    }else
    {
        let list=await Products.findAll({order:['id']})
        ctx.body=list;
    }



}
let fn_post= async(ctx,next)=>
{
    let obj=ctx.request.body

    Products.create(obj)
    ctx.body=
    {
        code:200,
        msg:'成功'
    }

}
let fn_put= async(ctx,next)=>
{
    let id=ctx.request.params.id
    let obj=ctx.request.body
    Products.update(obj,{where:{id:id}})
    ctx.body=
    {
        code:200,
        msg:'成功'
    }


}
let fn_delete= async(ctx,next)=>
{
    let id=ctx.request.params.id

    Products.destroy({where:{id:id}})
    ctx.body=
    {
        code:200,
        msg:'成功'
    }


}
module.exports=
{
    'get /product':fn_get,
    'post /product':fn_post,
    'put /product/:id':fn_put,
    'delete /product/:id':fn_delete
}