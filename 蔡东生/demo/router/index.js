'use strict'

const router=require('koa-router')()

const obj=require('./product.js')

for(let key in obj)
{
    let arr=key.split(' ')
    let fn=obj[key]
    if(arr[0]==='get')
    {
        router.get(arr[1],fn)
    }
    else if(arr[0]==='post')
    {
        router.post(arr[1],fn)
    }
    else if(arr[0]==='put')
    {
        router.put(arr[1],fn)
    }
    else if(arr[0]==='delete')
    {
        router.delete(arr[1],fn)
    }

}
module.exports=router.routes();