'use strict'

const koa =require('koa')
const cors =require('koa-cors')
const router =require('./router')
const bodyparser =require('koa-bodyparser')

let app=new koa();

app.use(cors())

app.use(bodyparser())

app.use(router)

app.listen(8080)
