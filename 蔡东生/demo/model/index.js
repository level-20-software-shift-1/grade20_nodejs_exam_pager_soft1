'use strict'

const {Sequelize,Op,DataTypes}=require('sequelize')

const model=
{
    biaoti:
    {
        type:DataTypes.STRING,
        allowNull:false
    },
    zhayao:
    {
        type:DataTypes.STRING,
        allowNull:false
    },
    neirong:
    {
        type:DataTypes.STRING,
        allowNull:false
    },
    fenlei:
    {
        type:DataTypes.STRING,
        allowNull:false
    },
    zuozhe:
    {
        type:DataTypes.STRING,
        allowNull:false
    },
    shijian:
    {
        type:DataTypes.INTEGER,
        allowNull:false
    }

}
let sequelize=new Sequelize('postgres','postgres','85952622cds',
    {
        host:'dudiaojiangxue.top',
        dialect:'postgres'

    })
let Products=sequelize.define('product',model)
sequelize.sync({force:true})
.then(async () => {
    Products.bulkCreate([
        {
            biaoti:'无',
            zhayao:'1',
            neirong:'123',
            fenlei:'123',
            zuozhe:'132',
            shijian:456
        },
        {
            biaoti:'有',
            zhayao:'1',
            neirong:'123',
            fenlei:'123',
            zuozhe:'132',
            shijian:456
        },
        {
            biaoti:'无',
            zhayao:'1',
            neirong:'123',
            fenlei:'123',
            zuozhe:'132',
            shijian:456
        }
    

    ])
})

module.exports={Products,Op};