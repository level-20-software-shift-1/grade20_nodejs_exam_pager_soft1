$(function(){

})
function getListwithKeyword(keyword){
    keyword=keyword||'';
    console.log(keyword);
    getList(keyword).then(res=>{
        console.log(res);
        render(res.data)
    })
}
function render(data){
    $('.tbRow').remove();
    data.forEach(item => {
        let html=`
        <tr class="tbRow" key="${item.id}">
        <td>${item.title}</td>
        <td>${item.digest}</td>
        <td>${item.content}</td>
        <td>${item.classify}</td>
        <td>${item.author}</td>
        <td>${item.timedate}</td>
        <td>
            <input type="button" value="编辑" onclick="update(${item.id})">
            <input type="button" value="删除" onclick="del(${item.id})">
        </td>
    </tr>`
    $('#tb').append(html);
    });
   
}
function add(){
    location.href='./AddOrEdit.html'
}