function getList(keyword){
    return new Promise((resolve,reject)=>{
        $.get(`${baseUrl}/blog?keyword=`+keyword,data=>{
            resolve(data);
        })
    })
}
function getbyid(id){
    return new Promise((resolve,reject)=>{
        $.get(`${baseUrl}/blog/${id}`,data=>{
            resolve(data);
        })
    })
}
function addBlog(obj){
    return new Promise((resolve,reject)=>{
        $.post(`${baseUrl}/blog`,obj,data=>{
            resolve(data);
        })
    })
}
function updateBlog(id,obj){
    return new Promise((resolve,reject)=>{
        $.ajax({
            url:`${baseUrl}/blog/${id}`,
            type:"PUT",
            data:obj,
            success:function(data){
                resolve(data);
            }
        })
    })
}
function deleteBlog(id){
    return new Promise((resolve,reject)=>{
        $.ajax({
            url:`${baseUrl}/blog/${id}`,
            type:"delete",
            success:function(data){
                resolve(data);
            }
        })
    })
}