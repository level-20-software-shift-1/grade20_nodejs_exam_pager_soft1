'use strict'
const {Sequelize,Op}=require('sequelize');
const fs=require('fs');
const sequelize=new Sequelize('demosoft1','postgres','1234@LIUliu',{
    host:'liu123.online',
    dialect:'postgres'
})
const file=fs.readdirSync(__dirname);
const filePath=file.filter(item=>{
    return item.endsWith('.js')&&item!=='index.js'
});
let resultModel={};
filePath.forEach(item=>{
    let modelName=item.replace('.js','');
    let model=require(__dirname+'/'+item);
    let tableName=modelName.toLowerCase();
     resultModel[modelName]=sequelize.define(tableName,model);
})
resultModel.sequelize=sequelize;
resultModel.Op=Op;
resultModel.sync=async()=>{
    return await sequelize.sync({force:true})
}

module.exports=resultModel;