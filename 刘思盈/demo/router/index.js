'use strict'
const router=require('koa-router')();
const fs=require('fs');
 function a(filePath){
     let tmpPath=__dirname||filePath;
     let file=fs.readdirSync(tmpPath);
     let fileArr=file.filter(item=>{
         return item.endsWith('.js')&&item!=='index.js'
     })
     return fileArr;
 }
 function b(router,fileArr){
     fileArr.forEach(item => {
         let tmpPath=__dirname+'/'+item;
         let obj=require(tmpPath);
         for(let key in obj){
             let tmparr=key.split(' ');
             let routerUrl=tmparr[1];
             let  routerMethod=tmparr[0];
             let routerFunction=obj[key];
             if(routerMethod==='get'){
                 router.get(routerUrl,routerFunction);
             }else  if(routerMethod==='post'){
                router.post(routerUrl,routerFunction);
            }else  if(routerMethod==='put'){
                router.put(routerUrl,routerFunction);
            } else  if(routerMethod==='delete'){
                router.delete(routerUrl,routerFunction);
            }
         }
     });
     return router.routes();
 }

 module.exports=function(){
     let fileArr=a();
     let fn=b(router,fileArr);
     return fn;
 }