"use strict"

let {DataTypes}=require("sequelize")


let blogs = {
    title: {
        type: DataTypes.STRING,
        allowNull:false
    },
    abstract: {
        type : DataTypes.STRING,
        allowNull : false,
        default :0

    },
    content : {
        type : DataTypes.STRING,
        allowNull : false
    },
    classify : {
        type : DataTypes.STRING,
        allowNull : false
    },
    author : {
        type : DataTypes.STRING,
        allowNull : false
    },
  
    
}
module.exports=blogs