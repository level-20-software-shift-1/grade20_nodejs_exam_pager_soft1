"use strict"
const koa = require("koa");
const cors = require("koa-cors")
const bodyparser = require("koa-bodyparser")
const router = require("koa-router")()
let blog = require("./blogs");
const { Sequelize, Op } = require("sequelize");
const sequelize = new Sequelize("text1", "postgres", "Frx379520.", {
  host: "meetfang.top",
  dialect: "postgres"
});


let blogs=sequelize.define("blogs",blog);

sequelize.sync({force:true}).then(()=>{
    blogs.bulkCreate([
        {
            title: "小说",
            abstract: "这是一本小说",
            content: "这是小说内容",
            classify: "这是小说分类",
            author: "这是小说作者",
            

        },
         {
            title: "小说",
            abstract: "这是一本小说",
            content: "这是小说内容",
            classify: "这是小说分类",
            author: "这是小说作者",
          

        },
        {
            title: "小说",
            abstract: "这是一本小说",
            content: "这是小说内容",
            classify: "这是小说分类",
            author: "这是小说作者",
          
        },
    ]
    )
})
let app =new koa();

router.get("/blogs", async (ctx) => {
    let list = await blogs.findAll({
      order: ["id"]
    })
    ctx.body = {
      data: list
    }
  })

//新增
router.post("/blogs", async (ctx) => {
    let { title,abstract,content,classify,author } = ctx.request.body
    await blogs.create({ title:title,abstract:abstract,content:content,classify:classify,author:author })
  
    ctx.body = "新增成功"
  
  })
  //查询指定id
  router.get("/blogs/:id", async (ctx) => {
    let { id } = ctx.request.params;
    console.log(id);
    let list = await blogs.findAll({
      where: {
        id: id
      }
    });
    ctx.body = list
  
  })
//查关键字
  router.post("/blogs/keyword",async (ctx)=>{

    let keyword = ctx.request.body.keyword || '';
    console.log(keyword.length);
    //判断关键字是否为空
    let list;
    if (keyword) {
        list = await blogs.findAll({
            where: {
                [Op.or]: [
                    { id: keyword },
                    { title: keyword },
                    { abstract: keyword },
                    { content:  keyword },
                    { classify:  keyword},
                    { author: keyword },
                ]
            },
            order:['id']
        })


    }
    ctx.body=list;
  })
  //更新
  router.put("/blogs/id", async (ctx) => {
    let { id, title,abstract,content,classify,author } = ctx.request.body
   await blogs.findAll({
      where: {
        id: id
      }
    })
    if (title !== undefined) {
      await blogs.update({ title:title,abstract:abstract,content:content,classify:classify,author:author }, {
        where: {
          id: id
        }
      })
    }
    ctx.body = "更新成功";
  })
  //删除
  router.delete("/blogs/:id", async (ctx) => {
    let id = ctx.request.params.id;
    console.log(id);
    await blogs.destroy({
      where: {
        id: id
      }
    });
  
    ctx.body = '删除成功';
  })

app.use(cors());
app.use(bodyparser())
app.use(router.routes())


app.listen(3000)

console.log("http://localhost:3000");