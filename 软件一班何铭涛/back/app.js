'use strict';

const Koa = require('koa');
const router = require('koa-router')();
const bodyparser = require('koa-bodyparser');
const cors = require('cors')
const Sequelize= require('sequelize');
const product = require('./model/Product')
let app = new Koa();


let sequelize = new Sequelize('postgres','postgres','123',{
    host:'hmtsure.top',     
    dialect:'postgres'
})


let Products = sequelize.define('products',product);

sequelize.sync({force:true}).then(()=>{
    Products.bulkCreate([
        {
        id:1,
        headline: '论efcore的自我修养',
        abstract: '论efcore的自我修养',
        body: '论efcore的自我修养',
        classify: '.Net',
        author:'InCerry',
        time:'2022-04-06 08:47'

        },
        {
            id:2,
            headline: 'DDD之我见',
            abstract: 'DDD之我见',
            body: 'DDD之我见',
            classify: '编程技术',
            author:'某大神',
            time:'2022-04-03 23：47'
        },
        {   
            id:3,
            headline: 'nginx负载平衡的几种策略',
            abstract: 'nginx负载平衡的几种策略',
            body: 'nginx负载平衡的几种策略',
            classify: '服务器',
            author:'老胡来也',
            time:'2022-04-06 08:47'
        },
        {
            id:4,
            headline: 'linux用户创建的学习研究',
            abstract: 'linux用户创建的学习研究',
            body: 'linux用户创建的学习研究',
            classify: 'linux',
            author:'某大神',
            time:'2022-04-06 08:47'
        },
        {
            id:5,
            headline: '大数据仪表盘探讨',
            abstract: '大数据仪表盘探讨',
            body: '大数据仪表盘探讨',
            classify: '大数据',
            author:'居家博士',
            time:'2022-04-06 08:47'
        },

    ])
});


let products = [
    {   
        id:1,
        headline: '论efcore的自我修养',
        abstract: '论efcore的自我修养',
        body: '论efcore的自我修养',
        classify: '.Net',
        author:'InCerry',
        time:'2022-04-06 08:47'

        },
        {
            id:2,
            headline: 'DDD之我见',
            abstract: 'DDD之我见',
            body: 'DDD之我见',
            classify: '编程技术',
            author:'某大神',
            time:'2022-04-03 23：47'
        },
        {   
            id:3,
            headline: 'nginx负载平衡的几种策略',
            abstract: 'nginx负载平衡的几种策略',
            body: 'nginx负载平衡的几种策略',
            classify: '服务器',
            author:'老胡来也',
            time:'2022-04-06 08:47'
        },
        {
            id:4,
            headline: 'linux用户创建的学习研究',
            abstract: 'linux用户创建的学习研究',
            body: 'linux用户创建的学习研究',
            classify: 'linux',
            author:'某大神',
            time:'2022-04-06 08:47'
        },
        {
            id:5,
            headline: '大数据仪表盘探讨',
            abstract: '大数据仪表盘探讨',
            body: '大数据仪表盘探讨',
            classify: '大数据',
            author:'居家博士',
            time:'2022-04-06 08:47'
        }
]
//首页
router.get('/product',(ctx,next)=>{
    ctx.request.body=product
})

//增

router.post('/product',async(ctx,next)=>{
    let obj = ctx.request.body;
await Products.create(obj);
ctx.body = obj;
});

//删
router.delete('/product/:id',async(ctx,next)=>{
    let id = ctx.request.params.id;
let product = await Products.findByPk(id);
//找到id后开始比较
if (product) {
    // products = arr;
    Products.destroy({
        where:{
            id:id
        }
    })
    ctx.body = {
        code: 1000,
        data: { id },
        msg: '删除成功'
    }
} else {
    ctx.body = {
        code: 999,
        data: null,
        msg: '要删除的元素或者记录不存在，请确认重试。'
    }
}
});
//改

router.put('/product/:id',async(ctx,next)=>{
    let id = ctx.request.params.id;
    let obj = ctx.request.body;
    let product = await Products.findByPk(id);
    //获得id，如果对应上了就开始判断
    
    if (product) {
        Products.update(obj,{
            where:{
                id:id
            }
        })
        ctx.body = {
            code: 1000,
            data: product,
            msg: '修改成功'
        }
    } else {
        ctx.body = {
            code: 999,
            data: '',
            msg: '你所需要修改的商品不存在'
        }
    }
});
//查

router.get('/product/:id',async(ctx,next)=>{
    let id = ctx.request.params.id;
    //获得用户输入的id
let product = await Products.findByPk(id);
//找到后开始判定
if (product) {
    ctx.body = {
        code: 1000,
        data: product,
        msg: '获取指定商品成功'
    }
} else {
    ctx.body = {
        code: 999,
        data: '',
        msg: '你查找的商品不存在'
    }
}
});


app.use(cors());
app.use(bodyparser());
app.use(router.routes());

let port = 8000;
app.listen(port);
console.log(`服务器运行在如下地址：http://localhost:${port}`);