'use strict';
let{DataTypes}=require('sequelize');

let model = {
    headline: {
        type: DataTypes.STRING,
        allowNull: false
    },
    abstract:{
        type:DataTypes.DECIMAL,
        allowNull:false,
    
    },
    body:{
        type:DataTypes.INTEGER,
        allowNull:false,
        
    },
    classify:{
        type:DataTypes.STRING,
        allowNull:false
    },
    author:{
        type:DataTypes.STRING,
        allowNull:false
    },
    time:{
        type:DataTypes.STRING,
        allowNull:false
    },
}

module.exports=model;