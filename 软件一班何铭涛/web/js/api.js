'use strict';

// 展示博客首页内容
function getProductList(){
    return new Promise(function(resolve,reject){
        $.get('http://localhost:8000/product',(data)=>{
            resolve(data)
        })
    })
}
// 查询博客内容
function getProductById(id) {
    return new Promise(function (res, rej) {
        $.get(`https://localhost:8000/product/${id}`, data => {
            res(data);
        })
    })
}

// 新增博客信息
function addProduct(obj) {
    return new Promise(function (resolve, reject) {
        $.post(`https://localhost:8000/product`, obj, (data) => {
            resolve(data);
        })
    })
}
// 更新博客信息
function updateProduct(id, obj) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `https://localhost:8000/product/${id}`,
            type: "PUT",
            data: obj,
            success: function (data) {
                resolve(data);
            }
        });
    })
}
// 删除指定的博客
function delProductById(id) {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: `https://localhost:8000/product/${id}`,
            type: "delete",
            success: function (data) {
                resolve(data);
            }
        });
    })
}
