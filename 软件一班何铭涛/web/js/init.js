'use strict';
$(function () {
    getProductList().then(data=>{
        let tb = $('#tbData');
        let rowData = $('.rowData');
        rowData.remove();
        data.forEach(item => {
            let h = `
                <tr class="rowData" key="${item.id}">
                    <td>${item.id}</td>
                    <td>${item.headline}</td>
                    <td>${item.abstract}</td>
                    <td>${item.body}</td>
                    <td>${item.classify}</td>
                    <td>${item.author}</td>
                    <td>${item.time}</td>
                    <td>
                    <input type="button" value="编辑" onclick="update(${item.id})">
                    <input type="button" value="删除" onclick="del(${item.id})">
                    </td>
                </tr>
                `
            tb.append(h);
            })
        })
})
//添加按钮的方法
function add() {
    location.href = './addOrEdit.html';
}


