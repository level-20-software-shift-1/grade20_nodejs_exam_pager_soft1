'use strict'

const {DataTypes}=require('sequelize');

let modul = {
    id:{
        type:DataTypes.INTEGER,
        allowNull:false
    },
    headline:{//标题
        type:DataTypes.STRING,
        allowNull:false
    },
    abstract:{//摘要
        type:DataTypes.STRING,
        allowNull:false
    },
    content:{//内容
        type:DataTypes.STRING,
        allowNull:false
    },
    classify:{//分类
        type:DataTypes.STRING,
        allowNull:false
    },
    author:{//作者
        type:DataTypes.STRING,
        allowNull:false
    },
    time:{//时间
        type:DataTypes.time,
        allowNull:false
    }
    

};

module.exports = modul;