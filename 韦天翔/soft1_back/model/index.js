'use strict'

const fs = require('fs');
const { db_dialect, db_username, db_host, db_password, db_database } = require('../config/db');
const { Sequelize, DataTypes, Op } = require('sequelize');

const sequelize = new Sequelize(db_database, db_username, db_password, {
    host: db_host,
    dialect: db_dialect
});

let files = fs.readdirSync(__dirname);

let obj = files.filter(item => {
    return item.endsWith('.js') && item != 'index.js';
})

let resultModul = {};

console.log('111');
console.log(obj);
obj.forEach(item => {
    let model = require(__dirname + '/' + item);
    let modulName = item.replace('.js', '');
    resultModul[modulName] = sequelize.define(modulName.toLowerCase(), model);
});

resultModul.sync = async () => {
    return await sequelize.sync({ force: ture });
};

resultModul.sequelize = sequelize;
resultModul.Op = Op;

module.exports = resultModul;