'use strict'

const Koa = require('koa');
const bodyparser = require('koa-bodyparser');

const router = require('./router');
const Cors = require('koa-cors');
const {Boke,sync} = require('./model');


sync().then(()=>{
    Boke.bulkCreate([
        {
            id: 1,
        headline: '论EF Core的自我修养',
        abstract: 'Core的自我修养',
        content: '论EF Core的自我修养',
        classify: '.net',
        author:'In Cerry',
        time:'2022-04-06-08.47'
        },
        {
            id: 2,
            headline: 'DDD',
            abstract: 'DDD',
            content: '论EF DDD',
            classify: '技术',
            author:'In DDD',
            time:'2022-05-06-08.47'
        },
    ])
})

let app = new Koa();
app.use(Cors());
app.use(bodyparser());
app.use(router());

let port = 8080;
app.listen(port);
console.log(`http://localhost:${port}`);