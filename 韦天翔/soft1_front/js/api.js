'use strict';

// 获取数据
function getBokeList(keyword){
    return new Promise(function(resolve,reject){
        $.get(`${baseUrl}/boke?keyword=${keyword}`,data=>{
            resolve(data);
        })
    })
}

// 根据id查找
function getListById(id){
    return new Promise(function(resolve,reject){
        $.get(`${baseUrl}/boke/${id}`,data=>{
            resolve(data);
        })
    })
}
// 删除商品
function delBokeById(id){
    return new Promise(function(resolve,reject){
        $.ajax({
            url:`${baseUrl}/boke/${id}`,
            type:'delete',
            success:function(data){
                resolve(data)
            }
        })
    })
}

